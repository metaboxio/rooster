<?php get_header(); ?>

<div class="page-header">
	<div class="container">
		<h1 class="page-title">Powerful features for your custom fields</h1>
		<p class="archive-description">Save &gt; 70% with extension bundles.</p>
		<p class="archive-cta"><a href="/pricing/" class="button">View Bundles</a></p>
	</div>
</div>

<main class="container">
	<ul class="filter">
		<li class="active" data-filter="">All</li>
		<?php
		$terms = get_terms( [
			'taxonomy' => 'product_category',
			'exclude'  => [483],
		] );
		foreach ( $terms as $term ) {
			echo '<li data-filter="' . esc_attr( $term->slug ) . '">' . esc_html( $term->name ) . '</li>';
		}
		?>
	</ul>

	<div class="products grid grid--3">
		<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/content', 'product' );

		endwhile;
		?>
	</div>
</main>

<script>
( function( w, d ) {
	let items = [ ...d.querySelectorAll( '.product' ) ],
		show = item => item.classList.remove( 'is-hidden' ),
		hide = item => item.classList.add( 'is-hidden' );

	d.querySelector( '.filter' ).addEventListener( 'click', ( { target: { dataset: { filter } } } ) => {
		items.map( show );
		items.filter( item => filter && !item.classList.contains( filter ) ).map( hide );
	}, { passive: true } );
} )( window, document );
</script>

<?php
get_footer();
