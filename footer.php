	</div>

	<footer id="colophon" class="site-footer">
		<div class="container">
			<?php if ( is_active_sidebar( 'sidebar-2' ) ) : ?>
				<div class="footer-widgets grid grid--4">
					<?php dynamic_sidebar( 'sidebar-2' ); ?>
				</div>
			<?php endif; ?>
			<div class="site-info">
				<div class="credit">Copyright <?php echo date( 'Y' ); ?> Meta Box. <a href="/privacy-policy/">Privacy Policy</a> | <a href="/terms-conditions/">Terms &amp; Conditions</a></div>
				<a class="company" href="https://elightup.com"><img src="<?= rooster_image_cdn( 'images/elightup.png' ) ?>" alt="elightup" width="54" height="16"></a>
			</div>
		</div>
	</footer>
</div>

<script>
( function( d ) {
	d.querySelector( '.menu-toggle' ).addEventListener( 'click', () => {
		d.querySelector( '#site-navigation' ).classList.toggle( 'toggled' );
	}, { passive: true } );
} )( document );
</script>

<?php wp_footer(); ?>

</body>
</html>
