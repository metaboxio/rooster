<?php
get_header();

get_template_part( 'template-parts/home/hero' );
get_template_part( 'template-parts/home/intro' );
get_template_part( 'template-parts/home/rating' );
get_template_part( 'template-parts/home/stats' );
get_template_part( 'template-parts/home/features-1' );
get_template_part( 'template-parts/home/features-2' );
get_template_part( 'template-parts/home/features-3' );
get_template_part( 'template-parts/home/extensions' );
get_template_part( 'template-parts/home/testimonials' );
get_template_part( 'template-parts/home/cta' );
get_template_part( 'template-parts/home/clients' );
get_template_part( 'template-parts/home/subscription' );
?>

<script>
( function( w, d ) {
	let top = d.querySelector( '#rating' ).getBoundingClientRect().top - 76;
	d.querySelectorAll( '.jump' ).forEach( l => l.addEventListener( 'click', () => w.scrollTo( { top, left: 0, behavior: 'smooth' } ), { passive: true } ) );
} )( window, document );
</script>

<?php
get_footer();
