<?php
function rooster_setup() {
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'title-tag' );

	add_theme_support( 'post-thumbnails' );
	add_image_size( 'rooster-product', 360, 215, true );
	set_post_thumbnail_size( 718, 300, true );

	register_nav_menus( array(
		'menu-1' => esc_html__( 'Primary', 'rooster' ),
		'menu-2' => esc_html__( 'Top Bar Left', 'rooster' ),
		'menu-3' => esc_html__( 'Top Bar Right', 'rooster' ),
	) );

	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	add_theme_support( 'customize-selective-refresh-widgets' );
	add_theme_support( 'custom-logo' );
}
add_action( 'after_setup_theme', 'rooster_setup' );

function rooster_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'rooster_content_width', 718 );
}
add_action( 'after_setup_theme', 'rooster_content_width', 0 );

function rooster_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'rooster' ),
		'id'            => 'sidebar-1',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer', 'rooster' ),
		'id'            => 'sidebar-2',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Forum', 'rooster' ),
		'id'            => 'sidebar-3',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Documentation', 'rooster' ),
		'id'            => 'sidebar-4',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Products', 'rooster' ),
		'id'            => 'sidebar-5',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	include __DIR__ . '/inc/widgets/product-purchase.php';
	include __DIR__ . '/inc/widgets/product-details.php';
	include __DIR__ . '/inc/widgets/product-related.php';

	register_widget( 'Rooster_Product_Purchase_Widget' );
	register_widget( 'Rooster_Product_Details_Widget' );
	register_widget( 'Rooster_Product_Related_Widget' );
}
add_action( 'widgets_init', 'rooster_widgets_init' );

function rooster_scripts() {
	wp_enqueue_style( 'rooster', get_stylesheet_uri(), [], filemtime( __DIR__ . '/style.css' ) );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'rooster_scripts' );

require __DIR__ . '/inc/template-tags.php';
require __DIR__ . '/inc/template-functions.php';
require __DIR__ . '/inc/icon-functions.php';
