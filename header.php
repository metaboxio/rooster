<!doctype html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">

<style>
@font-face {
	font-family: "Roboto Slab";
	font-weight: 700;
	font-display: swap;
	src:
		local("Roboto Slab"),
		url(/wp-content/themes/rooster/fonts/roboto-slab.woff2) format('woff2');
}
</style>

<?php if ( is_front_page() ) : ?>
	<link rel="preload" as="image" href="https://i0.wp.com/metabox.io/wp-content/themes/rooster/images/hero-background.png">
	<link rel="preload" as="image" href="https://i2.wp.com/metabox.io/wp-content/uploads/2017/12/wordpress-custom-fields.png?fit=730%2C493&quality=100&ssl=1">
<?php endif; ?>

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?><?php if ( is_singular( 'product' ) ) echo ' itemscope itemtype="http://schema.org/Product"'; ?>>
<div id="page" class="site">
	<?php do_action( 'rooster_header_before' ) ?>
	<?php get_template_part( 'template-parts/top-bar' ); ?>

	<header class="site-header">
		<div class="site-branding">
			<?php $tag = is_front_page() || ! is_singular() ? 'h1' : 'p'; ?>
			<<?= $tag ?> class="site-title">
				<a href="/" rel="home">
					<!--
					<img src="https://i0.wp.com/metabox.io/wp-content/uploads/2020/12/logo-xmas.png" alt="Meta Box Logo Xmax" width="182" height="46">
					<meta itemprop="brand" content="<?php bloginfo( 'name' ); ?>">
					-->

					<svg viewBox="227 227 46 46" width="46" height="46" xmlns="http://www.w3.org/2000/svg">
					  <rect style="" x="227" y="227" width="46" height="46" rx="4" ry="4"/>
					  <path d="M 256.873 255.508 L 257.283 246.088 L 257.193 246.078 L 251.483 261.868 L 248.613 261.868 L 242.933 246.138 L 242.843 246.148 L 243.253 255.508 L 243.253 258.868 L 245.533 259.308 L 245.533 261.868 L 236.703 261.868 L 236.703 259.308 L 238.973 258.868 L 238.973 243.558 L 236.703 243.118 L 236.703 240.538 L 238.973 240.538 L 244.553 240.538 L 249.993 256.258 L 250.073 256.258 L 255.553 240.538 L 263.433 240.538 L 263.433 243.118 L 261.153 243.558 L 261.153 258.868 L 263.433 259.308 L 263.433 261.868 L 254.603 261.868 L 254.603 259.308 L 256.873 258.868 L 256.873 255.508 Z" style="fill:#fff"/>
					</svg>
					<span<?php if ( is_singular( 'product' ) ) echo ' itemprop="brand"'; ?>><?php bloginfo( 'name' ); ?></span>
				</a>
			</<?= $tag ?>>
			<p class="site-description"><?= get_bloginfo( 'description', 'display' ) ?></p>
		</div>

		<nav id="site-navigation" class="main-navigation header-navigation">
			<button class="menu-toggle"><?php esc_html_e( 'Menu', 'rooster' ); ?></button>
			<?php
			wp_nav_menu( [
				'theme_location' => 'menu-1',
				'menu_id'        => 'primary-menu',
				'container'      => '',
			] );
			?>
		</nav>
	</header>

	<div class="site-content">
