<?php
/**
 * SVG icons related functions and filters
 *
 * @package Rooster
 */

/**
 * Return SVG markup.
 *
 * @param string $icon Icon name
 * }
 * @return string SVG markup.
 */
function rooster_get_svg( $icon ) {
	// Set aria hidden.
	$aria_hidden = ' aria-hidden="true"';

	// Set ARIA.
	$aria_labelledby = '';

	// Begin SVG markup.
	$svg = '<svg class="icon icon-' . esc_attr( $icon ) . '"' . $aria_hidden . $aria_labelledby . ' role="img">';

	/*
	 * Display the icon.
	 *
	 * The whitespace around `<use>` is intentional - it is a work around to a keyboard navigation bug in Safari 10.
	 *
	 * See https://core.trac.wordpress.org/ticket/38387.
	 */
	$svg .= ' <use href="#icon-' . esc_html( $icon ) . '" xlink:href="#icon-' . esc_html( $icon ) . '"></use> ';

	$svg .= '</svg>';

	return $svg;
}

/**
 * Output inline SVG markup.
 *
 * @param string $name Icon name.
 */
function rooster_inline_svg( $name ) {
	include get_template_directory() . "/images/$name.svg";
}
