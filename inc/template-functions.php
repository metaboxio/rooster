<?php
function rooster_post_classes( $classes ) {
	if ( is_singular() ) {
		$classes = array_diff( $classes, array( 'hentry' ) );
	}
	return $classes;
}
add_filter( 'post_class', 'rooster_post_classes' );
