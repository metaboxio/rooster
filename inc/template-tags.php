<?php
function rooster_posted_on() {
	printf(
		'<span class="author vcard screen-reader-text"><a class="url fn n" href="%s">%s</a></span>',
		esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
		esc_html( get_the_author() )
	);

	$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time>';
	if ( get_the_date( 'U' ) !== get_the_modified_date( 'U' ) ) {
		$time_string = '<time class="updated" datetime="%3$s">Updated: %4$s</time>' . $time_string;
	}

	printf(
		$time_string, // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
		esc_attr( get_the_date( DATE_W3C ) ),
		esc_html( get_the_date() ),
		esc_attr( get_the_modified_date( DATE_W3C ) ),
		esc_html( get_the_modified_date() )
	);
}

function rooster_image_cdn( $file ) {
	$url = get_parent_theme_file_uri( $file );
	$url = str_replace( [ 'https://', 'http://' ], '', $url );
	$url = str_replace( 'localhost/metaboxio', 'metabox.io', $url );
	$url = add_query_arg( 'quality', 100, "https://i0.wp.com/$url" );
	return $url;
}

function rooster_sharing_buttons() {
	$services = [
		'facebook' => [
			'title' => 'Share on Facebook',
			'url'   => 'https://facebook.com/sharer/sharer.php?u=%s',
		],
		'twitter' => [
			'title' => 'Share on Twitter',
			'url'   => 'https://twitter.com/intent/tweet?url=%s',
		],
		'linkedin' => [
			'title' => 'Share on LinkedIn',
			'url'   => 'https://www.linkedin.com/shareArticle?mini=true&url=%s',
		],
		'reddit' => [
			'title' => 'Share on Reddit',
			'url' => 'https://www.reddit.com/submit?url=%s',
		],
	];

	$blocks = '<!-- wp:social-links {"openInNewTab":true} --><ul class="wp-block-social-links sharing-buttons">';
	foreach ( $services as $key => $service ) {
		$attributes = [
			'service' => $key,
			'url'     => sprintf( $service['url'], get_permalink() ),
			'label'   => $service['title'],
		];
		$blocks .= '<!-- wp:social-link ' . wp_json_encode( $attributes ) . ' /-->';
	}
	$blocks .= '</ul><!-- /wp:social-links -->';

	echo do_blocks( $blocks );
}

function rooster_related_posts( int $number = 3, array $args = [] ) : ?WP_Query {
	if ( ! class_exists( 'Jetpack_RelatedPosts' ) ) {
		return null;
	}

	$related = Jetpack_RelatedPosts::init_raw()->get_for_post_id( get_the_ID(), [
		'size' => $number
	] );
	if ( empty( $related ) ) {
		return null;
	}
	$related = wp_list_pluck( $related, 'id' );

	$args = wp_parse_args( $args, [
		'post_status'            => 'publish',
		'posts_per_page'         => $number,
		'post__in'               => $related,
		'no_found_rows'          => true,
		'update_post_term_cache' => false,
		'ignore_sticky_posts'    => true,
	] );

	return new WP_Query( $args );
}