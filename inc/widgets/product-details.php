<?php
class Rooster_Product_Details_Widget extends WP_Widget {
	public $defaults = [
		'title' => 'Details',
	];

	public function __construct() {
		parent::__construct( 'rooster-product-details', 'Rooster: Product Details', ['classname' => 'details'] );
	}

	public function widget( $args, $instance ) {
		$instance = wp_parse_args( $instance, $this->defaults );

		echo $args['before_widget'];

		if ( $instance['title'] ) {
			echo $args['before_title'] . $instance['title'] . $args['after_title'];
		}
		?>
		<ul>
			<li class="detail">
				<div class="detail__label">Version</div>
				<div class="detail__label"><?= get_post_meta( get_the_ID(), 'version', true ); ?></div>
			</li>
			<li class="detail">
				<div class="detail__label">Compatibility</div>
				<div class="detail__label"><?= get_post_meta( get_the_ID(), 'compatibility', true ); ?></div>
			</li>
			<?php
			the_date( '', '<li class="detail"><div class="detail__label">Release Date</div><div class="detail__value">', '</div></li>' );
			the_modified_date( '', '<li class="detail"><div class="detail__label">Last Update</div><div class="detail__value">', '</div></li>' );
			?>
			<li class="detail">
				<div class="detail__label">Changelog</div>
				<div class="detail__label"><a href="<?php the_permalink(); ?>changelog/">View</a></div>
			</li>
		</ul>
		<?php
		echo $args['after_widget'];
	}

	public function update( $new_instance, $old_instance ) {
		$instance          = array();
		$instance['title'] = strip_tags( $new_instance['title'] );
		return $instance;
	}

	public function form( $instance ) {
		$instance = wp_parse_args( $instance, $this->defaults );
		?>
		<p>
			<label for="<?= esc_attr( $this->get_field_id( 'title' ) ); ?>">Title:</label>
			<input class="widefat" id="<?= esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?= esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?= esc_attr( $instance['title'] ); ?>">
		</p>
		<?php
	}
}
