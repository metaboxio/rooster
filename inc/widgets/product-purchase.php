<?php
class Rooster_Product_Purchase_Widget extends WP_Widget {
	public function __construct() {
		parent::__construct( 'rooster-product-purchase', 'Rooster: Product Purchase', ['classname' => 'purchase'] );
	}

	public function widget( $args, $instance ) {
		echo $args['before_widget'];
		?>
		<?= $args['before_title']; ?>Get The Extension<?= $args['after_title']; ?>
		<div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
			<meta itemprop="ratingValue" content="4.9">
			<meta itemprop="ratingCount" content="106">
		</div>
		<div itemprop="review" itemtype="http://schema.org/Review" itemscope>
			<meta itemprop="reviewBody" content="Best custom fields framework. Dedicated developer.">
			<div itemprop="author" itemtype="http://schema.org/Person" itemscope>
				<meta itemprop="name" content="valuebg">
			</div>
			<div itemprop="reviewRating" itemtype="http://schema.org/Rating" itemscope>
				<meta itemprop="ratingValue" content="5">
			</div>
		</div>
		<div itemprop="review" itemtype="http://schema.org/Review" itemscope>
			<meta itemprop="reviewBody" content="Great Plugin with even Greater Support">
			<div itemprop="author" itemtype="http://schema.org/Person" itemscope>
				<meta itemprop="name" content="marriageheat">
			</div>
			<div itemprop="reviewRating" itemtype="http://schema.org/Rating" itemscope>
				<meta itemprop="ratingValue" content="5">
			</div>
		</div>
		<div itemprop="review" itemtype="http://schema.org/Review" itemscope>
			<meta itemprop="reviewBody" content="Best Plugin for Custom Fields">
			<div itemprop="author" itemtype="http://schema.org/Person" itemscope>
				<meta itemprop="name" content="sododesign">
			</div>
			<div itemprop="reviewRating" itemtype="http://schema.org/Rating" itemscope>
				<meta itemprop="ratingValue" content="5">
			</div>
		</div>
		<meta itemprop="sku" content="<?php the_ID() ?>">
		<meta itemprop="mpn" content="<?php the_ID() ?>">
		<?php if ( get_post_meta( get_the_ID(), 'free', true ) ) : ?>
			<div class="purchase__price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
				<link itemprop="availability" href="http://schema.org/InStock">
				<meta itemprop="priceCurrency" content="USD">
				<meta itemprop="price" content="0">
				<meta itemprop="priceValidUntil" content="<?= date( 'c', strtotime( '+1 year' ) ) ?>">
				<meta itemprop="url" content="<?php the_permalink() ?> ">
				FREE
			</div>
			<ul class="purchase__terms">
				<li><svg class="icon"><use xlink:href="#checkmark" /></svg> Unlimited sites</li>
				<li><svg class="icon"><use xlink:href="#checkmark" /></svg> Community support on Github</li>
			</ul>
			<?php
			$link = '<a href="' . get_post_meta( get_the_ID(), 'file', true ) . '" class="button button--block button--cyan">Download</a>';
			echo $link;
			?>
		<?php else : ?>
			<?php if ( has_term( 'lifetime-only', 'product_tag' ) ) : ?>
				<?php $this->output_lifetime_only_notice(); ?>
			<?php elseif ( has_term( 'bundle-only', 'product_tag' ) ) : ?>
				<?php $this->output_bundle_only_notice(); ?>
			<?php else : ?>
				<div class="purchase__price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
					<link itemprop="availability" href="http://schema.org/InStock">
					<meta itemprop="url" content="<?php the_permalink() ?> ">
					<meta itemprop="priceCurrency" content="USD">
					<?php
					$price = get_post_meta( get_the_ID(), 'price', true );
					$sale_price = floor( $price * 0.7 );
					?>

					<sup>$</sup><span itemprop="price"><?= $price; ?></span>
					<meta itemprop="priceValidUntil" content="<?= date( 'c', strtotime( '+1 year' ) ) ?>">
					<!--<sup>$</sup><span itemprop="price"><?= $sale_price; ?></span>-->
					<!-- <div class="purchase__price__old">was <sup>$</sup><s><?= $price ?></s></div> -->
				</div>
				<ul class="purchase__terms">
					<li><svg class="icon"><use xlink:href="#checkmark" /></svg> Unlimited sites</li>
					<li><svg class="icon"><use xlink:href="#checkmark" /></svg> 1 year of updates &amp; support</li>
				</ul>
				<!--<p style="color: #d91e18; font-style: normal; font-weight: bold">Coupon HPNY19 - 30% OFF. Valid until 01/05.</p>-->

				<?= do_shortcode( '[buy_button class="button button--block"]' ); ?>
			<?php endif ?>

			<?php if ( ! has_term( 'lifetime-only', 'product_tag' ) ) : ?>
				<div class="purchase__bundle">
					<?= $args['before_title']; ?>Get The Bundle<?= $args['after_title']; ?>
					<div class="purchase__badge">
						<img src="<?= get_theme_file_uri( 'images/badge.png' ); ?>" alt="view bundle options" width="104" height="213">
					</div>
					<ul class="purchase__terms">
						<li><svg class="icon"><use xlink:href="#checkmark" /></svg> <strong>All premium extensions</strong></li>
						<li><svg class="icon"><use xlink:href="#checkmark" /></svg> <strong>All future extensions</strong></li>
						<li><svg class="icon"><use xlink:href="#checkmark" /></svg> Unlimited sites</li>
						<li><svg class="icon"><use xlink:href="#checkmark" /></svg> 1 year of updates &amp; support</li>
					</ul>
					<a href="/pricing/" data-gtm-category="Extension Page CRO" data-gtm-action="Click View Bundles Button" data-gtm-label="<?php the_title_attribute(); ?>" data-gtm-value="0" class="button button--block">See Your Options</a>
				</div>
			<?php endif; ?>
		<?php endif; ?>

		<svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
			<symbol id="checkmark" viewBox="0 0 6594 6277.5">
				<path d="M2387 3364l-1495 -1485c-51,-51 -134,-50 -184,0l-670 674c-51,51 -51,134 0,185 754,749 1507,1498 2261,2246 51,51 134,51 185,0l4073 -4092c50,-51 50,-134 -1,-185l-674 -669c-51,-51 -134,-51 -185,0l-3310 3326z"/>
			</symbol>
		</svg>

		<?php $post = get_queried_object(); ?>
		<?php if ( $post->post_name === 'meta-box-updater' || ! get_post_meta( get_the_ID(), 'free', true ) ) : ?>
			<?php if ( ! has_term( 'lifetime-only', 'product_tag' ) ) : ?>
				<div class="purchase__docs"><a href="https://docs.metabox.io/extensions/<?= $post->post_name; ?>/" class="button button--block button--gray" target="_blank">Documentation</a></div>
			<?php endif; ?>
		<?php endif; ?>
		<?php
		echo $args['after_widget'];
	}

	public function update( $new_instance, $old_instance ) {
		return $new_instance;
	}

	public function form( $instance ) {}

	private function output_lifetime_only_notice() {
		$lifetime_id = 32245;
		// $lifetime_id = 6174;
		?>
		<p><em>This solution is available only for the Lifetime Bundle.</em></p>
		<h3>Get the Lifetime Bundle</h3>
		<div class="purchase__price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
			<link itemprop="availability" href="http://schema.org/InStock">
			<meta itemprop="url" content="<?php the_permalink() ?> ">
			<meta itemprop="priceCurrency" content="USD">
			<?php
			$price = get_post_meta( $lifetime_id, 'price', true );
			$sale_price = floor( $price * 0.7 );
			?>

			<sup>$</sup><span itemprop="price"><?= $price; ?></span>
			<meta itemprop="priceValidUntil" content="<?= date( 'c', strtotime( '+1 year' ) ) ?>">
			<!--<sup>$</sup><span itemprop="price"><?= $sale_price; ?></span>-->
			<!-- <div class="purchase__price__old">was <sup>$</sup><s><?= $price ?></s></div> -->
		</div>
		<ul class="purchase__terms">
			<li><svg class="icon"><use xlink:href="#checkmark" /></svg> All premium extensions</li>
			<li><svg class="icon"><use xlink:href="#checkmark" /></svg> All future extensions</li>
			<li><svg class="icon"><use xlink:href="#checkmark" /></svg> Unlimited sites</li>
			<li><svg class="icon"><use xlink:href="#checkmark" /></svg> Lifetime updates &amp; support</li>
		</ul>
		<!--<p style="color: #d91e18; font-style: normal; font-weight: bold">Coupon HPNY19 - 30% OFF. Valid until 01/05.</p>-->

		<?= do_shortcode( '[buy_button id="' . $lifetime_id . '" class="button button--block"]' ); ?>
		<?php
	}

	private function output_bundle_only_notice() {
		?>
		<em>This extension is available only for the Developer and Lifetime Bundles.</em>
		<?php
	}
}
