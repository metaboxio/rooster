<?php
class Rooster_Product_Related_Widget extends WP_Widget {
	public $defaults = [
		'title' => 'You might also like',
	];

	public function __construct() {
		parent::__construct( 'rooster-product-related', 'Rooster: Product Related', ['classname' => 'related'] );
	}

	public function widget( $args, $instance ) {
		$query = rooster_related_posts( 3, ['post_type' => 'product'] );
		if ( $query === null ) {
			return;
		}

		$instance = wp_parse_args( $instance, $this->defaults );

		echo $args['before_widget'];

		if ( $instance['title'] ) {
			echo $args['before_title'] . $instance['title'] . $args['after_title'];
		}

		while ( $query->have_posts() ) {
			$query->the_post();
			?>
			<div class="card">
				<a class="card__image" href="<?php the_permalink() ?>">
					<?php the_post_thumbnail( 'rooster-product' ) ?>
				</a>
				<div class="card__block">
					<h4 class="card__title"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h4>
					<div class="card__text"><?php the_excerpt(); ?></div>
				</div>
			</div>

			<?php
		}
		wp_reset_postdata();

		echo $args['after_widget'];
	}

	public function update( $new_instance, $old_instance ) {
		$instance          = [];
		$instance['title'] = strip_tags( $new_instance['title'] );
		return $instance;
	}

	public function form( $instance ) {
		$instance = wp_parse_args( $instance, $this->defaults );
		?>
		<p>
			<label for="<?= esc_attr( $this->get_field_id( 'title' ) ); ?>">Title:</label>
			<input class="widefat" id="<?= esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?= esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?= esc_attr( $instance['title'] ); ?>">
		</p>
		<?php
	}
}
