<?php get_header(); ?>

<?php if ( is_home() && ! is_front_page() ) : ?>
	<div class="page-header">
		<div class="container">
			<h1 class="page-title"><?php single_post_title(); ?></h1>
		</div>
	</div>
<?php endif; ?>

<div class="container clear">
	<main class="content-area">
		<?php
		if ( have_posts() ) {
			while ( have_posts() ) {
				the_post();
				get_template_part( 'template-parts/content' );
			}
			the_posts_navigation();
		} else {
			get_template_part( 'template-parts/content', 'none' );
		}

		do_action( 'rooster_after_loop' );
		?>
	</main>

	<?php get_sidebar(); ?>
</div>

<?php
get_footer();
