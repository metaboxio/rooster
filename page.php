<?php
get_header();
the_post();
?>

<div class="page-header">
	<div class="container">
		<?php the_title( '<h1 class="page-title entry-title">', '</h1>' ); ?>
	</div>
</div>

<div class="container clear">
	<main class="content-area entry-content">
		<?php the_content(); ?>
	</main>

	<?php
	if ( function_exists( 'is_bbpress' ) && is_bbpress() ) {
		get_sidebar( 'forum' );
	}
	?>
</div>

<?php
get_footer();
