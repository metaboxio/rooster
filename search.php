<?php get_header(); ?>

<div class="page-header">
	<div class="container">
		<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'rooster' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
	</div>
</div>

<div class="container clear">
	<main class="content-area">
		<p><a href="https://docs.metabox.io/?utm_source=banner-search&utm_medium=content"><?= wp_get_attachment_image( 21819, 'full' ) ?></a></p>
		<?php if ( have_posts() ) : ?>
			<?php
			while ( have_posts() ) {
				the_post();
				get_template_part( 'template-parts/content' );
			}
			the_posts_navigation();
			?>
			<p><a href="https://docs.metabox.io/?utm_source=banner-search&utm_medium=content"><?= wp_get_attachment_image( 21819, 'full' ) ?></a></p>
		<?php else : ?>
			<?php get_template_part( 'template-parts/content', 'none' ); ?>
		<?php endif; ?>
	</main>

	<?php get_sidebar(); ?>
</div>

<?php
get_footer();
