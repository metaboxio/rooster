<form role="search" method="get" class="search-form" action="/">
	<label>
		<span class="screen-reader-text">Search for:</span>
		<input type="search" class="search-field" placeholder="Search &hellip;" value="<?= get_search_query(); ?>" name="s">
	</label>
	<input type="hidden" name="post_type" value="post">
	<input type="submit" class="search-submit" value="Search">
</form>
