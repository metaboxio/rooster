<?php
$sidebar_name = apply_filters( 'rooster_sidebar', 'sidebar-1' );

if ( ! is_active_sidebar( $sidebar_name ) ) {
	return;
}
?>

<aside id="secondary" class="sidebar widget-area" role="complementary">
	<?php dynamic_sidebar( $sidebar_name ); ?>
</aside><!-- #secondary -->
