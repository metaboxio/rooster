<?php
get_header();
the_post();
?>

<div class="page-header">
	<div class="container">
		<?php the_title( '<h1 class="page-title entry-title" itemprop="name">', '</h1>' ); ?>
		<?php the_post_thumbnail( 'full', ['itemprop' => 'image', 'style' => 'display: none', 'alt' => the_title_attribute( 'echo=0' )] ) ?>

		<?php if ( $tagline = get_post_meta( get_the_ID(), 'tagline', true ) ) : ?>
			<p><?= $tagline; ?></p>
		<?php else : ?>
			<p><?= get_the_excerpt(); // Jetpack can't hook! ?></p>
		<?php endif; ?>
		<?php if ( get_post_meta( get_the_ID(), 'badge', true ) ) : ?>
			<div class="badge"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z"/></svg> <?php rwmb_the_value( 'badge' ); ?></div>
		<?php endif; ?>
		<?php
		if ( function_exists('yoast_breadcrumb') ) {
			yoast_breadcrumb( '<div class="screen-reader-text">','</div>' );
		}
		?>
	</div>
</div>

<div class="container clear">
	<main class="content-area entry-content" itemprop="description">
		<?php the_content(); ?>
	</main>

	<?php get_sidebar( 'product' ); ?>
</div>

<?php
get_footer();
