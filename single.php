<?php
get_header();
the_post();
?>

<div class="page-header">
	<div class="container">
		<?php the_title( '<h1 class="page-title entry-title">', '</h1>' ); ?>
		<?php if ( 'post' === get_post_type() ) : ?>
			<div class="entry-meta">
				<?php rooster_posted_on(); ?>
			</div>
		<?php endif; ?>
		<?php rooster_sharing_buttons() ?>
	</div>
</div>

<div class="container clear">
	<main class="content-area">
		<article class="entry-content"><?php the_content(); ?></article>

		<?php $query = rooster_related_posts() ?>
		<?php if ( $query !== null ) : ?>
			<div class="related-posts">
				<h4>You might also like</h4>
				<div class="related-posts__inner">
					<?php while ( $query->have_posts() ) : $query->the_post(); ?>
						<a href="<?php the_permalink() ?>">
							<?php the_post_thumbnail( [220, 123] ) ?>
							<?php the_title() ?>
						</a>
					<?php endwhile ?>
					<?php wp_reset_postdata() ?>
				</div>
			</div>
		<?php endif ?>

		<?php
		if ( comments_open() || get_comments_number() ) {
			comments_template();
		}
		?>
	</main>

	<?php get_sidebar(); ?>
</div>

<?php
get_footer();
