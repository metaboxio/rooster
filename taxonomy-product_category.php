<?php get_header(); ?>

<div class="page-header">
	<div class="container">
		<h1 class="page-title"><?= single_term_title() ?></h1>
		<?php if ( is_tax( 'product_category', 'solutions' ) ) : ?>
			<p class="archive-description">Ready-to-use solutions available only for Lifetime bundle licenses</p>
			<p class="archive-cta"><?= do_shortcode( '[buy_button id="6174" text="Get Lifetime Bundle"]' ); ?></p>
		<?php else : ?>
			<p class="archive-description">Save &gt; 70% with extension bundles.</p>
			<p class="archive-cta"><a href="/pricing/" class="button">View Bundles</a></p>
		<?php endif; ?>
	</div>
</div>

<main class="container">
	<div class="products grid grid--3">
		<?php
		while ( have_posts() ) : the_post();
			get_template_part( 'template-parts/content', 'product' );
		endwhile;
		?>
	</div>
</main>

<?php
get_footer();
