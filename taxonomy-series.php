<?php get_header(); ?>

<div class="page-header">
	<div class="container">
		<?php
		the_archive_title( '<h1 class="page-title">', '</h1>' );
		the_archive_description( '<div class="archive-description">', '</div>' );
		?>
	</div>
</div>

<div class="container clear">
	<main class="content-area">
		<?= do_shortcode( '[series_list_posts series="' . get_queried_object_id() . '"]' ); ?>

		<?php
		if ( have_posts() ) {
			while ( have_posts() ) {
				the_post();
				get_template_part( 'template-parts/content' );
			}

			the_posts_navigation();
		} else {
			get_template_part( 'template-parts/content', 'none' );
		}

		do_action( 'rooster_after_loop' );
		?>
	</main>

	<?php get_sidebar(); ?>
</div>

<?php
get_footer();
