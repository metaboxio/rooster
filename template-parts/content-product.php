<?php
$class = ['product', 'card'];
$terms = get_the_terms( get_the_ID(), 'product_category' );

if ( $terms && ! is_wp_error( $terms ) ) {
	foreach ( $terms as $term ) {
		$class[] = $term->slug;
	}
}
?>
<div class="<?= esc_attr( implode( ' ', $class ) ); ?>">
	<a class="card__image" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
		<?php the_post_thumbnail( 'rooster-product' ); ?>
	</a>
	<div class="card__block">
		<h3 class="card__title"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
		<div class="card__text"><?php the_excerpt(); ?></div>
	</div>
</div>
