<section class="section section--center clients">
	<div class="container">
		<h2 class="section__title">Trusted By Top Brands &amp; Agencies</h2>
		<img loading="lazy" src="<?= rooster_image_cdn( 'images/customers.jpg' ) ?>" alt="clients" width="1140" height="36">
	</div>
</section>
