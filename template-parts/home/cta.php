<?php
/**
 * Template part for displaying cta 2 on homepage.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Rooster
 */

?>

<section class="section cta">
	<div class="container">
		<h2 class="section__title cta__heading">The Expert Choice for Building Custom Fields in WordPress</h2>
		<div class="cta__button"><a href="/pricing/" class="button button--cyan">Get Meta Box</a></div>
	</div>
</section>
