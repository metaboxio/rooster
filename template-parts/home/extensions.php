<section class="section section--center extensions">
	<div class="container">
		<h2 class="section__title">Extend custom fields in WordPress well beyond what others would ever consider ordinary</h2>
		<p class="section__description">Get everything you want in a custom fields plugin and nothing you don't with extensions. Mix, match and bundle 25+ extensions to add advanced functionality. Create a custom-fit with none of the bloat.</p>
		<p class="extensions__button"><a href="/pricing/" class="button button--cyan">View The Bundles</a></p>
		<div class="grid grid--3">
			<?php
			$query = new WP_Query( [
				'post_type'      => 'product',
				'posts_per_page' => 6,
				'post__in'       => [411, 355, 7, 29, 1559, 2073],
				'no_found_rows'  => true,
			] );
			while ( $query->have_posts() ) {
				$query->the_post();
				get_template_part( 'template-parts/content', 'product' );
			}
			wp_reset_postdata();
			?>
		</div>
	</div>
</section>
