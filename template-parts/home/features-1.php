<section class="section section--alt features">
	<div class="container clear">
		<div class="features__text">
			<h2 class="section__title features__title">So effortless, you're done at the same time as your coffee</h2>
			<h3 class="features__subtitle">Install in seconds. Create WordPress custom fields in minutes with our <a href="/online-generator/">Online Generator</a>.</h3>
			<p class="features__description">Choose your options, then copy and paste the automagically generated code to your website.</p>
			<div class="features__block grid grid--2">
				<div class="feature">
					<h3 class="feature__title"><a href="/online-generator/">Online Generator</a></h3>
					<p class="feature__description">Create your code through the Online Generator and save it for future projects.</p>
				</div>
				<div class="feature">
					<h3 class="feature__title"><a href="http://demo.metabox.io">Pre-Built Demos</a></h3>
					<p class="feature__description">Need ideas for getting started? Check out our code snippets on GitHub!</p>
				</div>
				<div class="feature">
					<h3 class="feature__title"><a href="/plugins/meta-box-builder/">Meta Box Builder</a></h3>
					<p class="feature__description">Drag and drop your custom fields onto the page — no coding knowledge required!</p>
				</div>
				<div class="feature">
					<h3 class="feature__title"><a href="/plugins/custom-post-type/">MB Custom Post Type</a></h3>
					<p class="feature__description">The free CPT Extension makes custom post types and taxonomies a snap.</p>
				</div>
			</div>
		</div>
		<div class="features__image">
			<div class="browser-bar"><span></span></div>
			<img loading="lazy" src="<?= rooster_image_cdn( 'images/online-generator.png' ) ?>" alt="meta box online generator" width="690" height="586">
		</div>
	</div>
</section>
