<section class="section features features--alt">
	<div class="container clear">
		<div class="features__text">
			<h2 class="section__title features__title">It's like an infinitely flexible WordPress custom fields plugin</h2>
			<h3 class="features__subtitle">Because it's exactly that. It's jam-packed with options so you can create a custom-fit solution that works.</h3>
			<p class="features__description">Meta Box is so highly customizable, it may as well look like you coded everything from scratch.</p>
			<div class="features__block grid grid--2">
				<div class="feature">
					<h3 class="feature__title"><a href="https://docs.metabox.io/field-settings/">Field Types</a></h3>
					<p class="feature__description">There are over 40 different options for data collection and they're all a snap to use.</p>
				</div>
				<div class="feature">
					<h3 class="feature__title"><a href="https://docs.metabox.io/field-settings/">Field Settings</a></h3>
					<p class="feature__description">Nothing is set in stone—configure each field to meet your exact needs.</p>
				</div>
				<div class="feature">
					<h3 class="feature__title"><a href="https://docs.metabox.io/extensions/mb-term-meta/">Every Data Type</a></h3>
					<p class="feature__description">No matter the data you need to display, (posts, attachments, terms, etc.) it can be done.</p>
				</div>
				<div class="feature">
					<h3 class="feature__title"><a href="https://docs.metabox.io/cloning-fields/">Cloning Fields</a></h3>
					<p class="feature__description">You determine the fields you add and you can reuse them with endless options.</p>
				</div>
			</div>
		</div>
		<div class="features__image">
			<div class="browser-bar"><span></span></div>
			<img loading="lazy" src="<?= rooster_image_cdn( 'images/customization.png' ) ?>" alt="customizable meta box plugin" width="690" height="680">
		</div>
	</div>
</section>
