<section class="section section--alt features">
	<div class="container clear">
		<div class="features__text">
			<h2 class="section__title features__title">It's a developer's best friend, but with no strings attached</h2>
			<h3 class="features__subtitle">Add only what you need and extend functionality where and when you want. You're in control.</h3>
			<p class="features__description">There are infinite options and extensions when you're ready.</p>
			<div class="features__block grid grid--2">
				<div class="feature">
					<h3 class="feature__title"><a href="http://deluxeblogtips.com/measure-php-project-complexity-maintainability/" target="_blank" rel="noopener noreferrer">Lightweight</a></h3>
					<p class="feature__description">No confusing admin or bloated interfaces. Files are loaded only when required.</p>
				</div>
				<div class="feature">
					<h3 class="feature__title"><a href="https://docs.metabox.io/integration/">Integrated</a></h3>
					<p class="feature__description">Meta Box integrates smoothly with any WordPress theme, plugin or website.</p>
				</div>
				<div class="feature">
					<h3 class="feature__title"><a href="https://docs.metabox.io/actions/">Hook, Line, Sinker</a></h3>
					<p class="feature__description">Add even more functionality with Meta Box's library of actions and filters.</p>
				</div>
				<div class="feature">
					<h3 class="feature__title"><a href="https://github.com/wpmetabox/meta-box/">Open Source</a></h3>
					<p class="feature__description">Meta Box is proudly open source and hosted on GitHub. So tinker away.</p>
				</div>
			</div>
		</div>
		<div class="features__image">
			<div class="browser-bar"><span></span></div>
			<img loading="lazy" src="<?= rooster_image_cdn( 'images/developer-friendly.png' ) ?>" alt="wordpress meta box plugin for developers" width="690" height="510">
		</div>
	</div>
</section>
