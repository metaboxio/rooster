<?php
/**
 * Template part for displaying hero on homepage.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Rooster
 */

the_post();
?>

<section class="hero showcase">
	<div class="container">
		<div class="hero__text showcase__text">
			<h2><?php the_title(); ?></h2>
			<?php the_content(); ?>
		</div>
		<div class="hero__image showcase__image">
			<?php the_post_thumbnail( 'full' ); ?>
		</div>
	</div>
</section>
