<section class="section section--center intro">
	<div class="container">
		<h2 class="section__title">A WordPress Custom Fields Plugin for Clever Custom Results</h2>
		<p class="section__description"><em>Meta Box</em> is a free Gutenberg and GDPR-compatible <strong>WordPress custom fields plugin and framework</strong> that makes quick work of customizing a website with&mdash;you guessed it&mdash;meta boxes and custom fields in WordPress.</p>
		<p class="section__description">There are <em>tons of options and extensions</em> to keep you busy or add only what you need. All the while keeping the load light with our API. It's also WordPress Multisite-compatible.</p>
		<div class="intro__more"><span class="jump">Learn more</span></div>
		<div class="intro__arrow"><span class="jump"><?php rooster_inline_svg( 'down-arrow' ); ?></span></div>
	</div>
</section>
