<section class="section section--alt section--center rating" id="rating">
	<div class="container">
		<div class="rating__block">
			<?php rooster_inline_svg( 'star' ); ?>
			<?php rooster_inline_svg( 'star' ); ?>
			<?php rooster_inline_svg( 'star' ); ?>
			<?php rooster_inline_svg( 'star' ); ?>
			<?php rooster_inline_svg( 'star' ); ?>
		</div>
		<div class="rating__block rating__block--first">
			<div class="rating__title">700,000+</div>
			<div class="rating__subtitle">active installs</div>
		</div>
		<div class="rating__block rating__block--second">
			<div class="rating__title">4.9/5</div>
			<div class="rating__subtitle">rating</div>
		</div>
	</div>
</section>
