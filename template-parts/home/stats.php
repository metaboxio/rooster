<section class="section section--center stats">
	<div class="container">
		<div class="grid grid--4">
			<div class="stat">
				<div class="stat__icon"><?php rooster_inline_svg( 'clock' ); ?></div>
				<h3 class="stat__title">SWIFT AND STRAIGHTFORWARD</h3>
				<div class="stat__text">No time? No problem! Meta Box saves you hours of tedious work and gives expert results.</div>
			</div>
			<div class="stat">
				<div class="stat__icon"><?php rooster_inline_svg( 'params' ); ?></div>
				<h3 class="stat__title">READY, SET, CUSTOMIZE</h3>
				<div class="stat__text">Forget pesky presets and workarounds. Build what you want, exactly as you want it.</div>
			</div>
			<div class="stat">
				<div class="stat__icon"><?php rooster_inline_svg( 'user' ); ?></div>
				<h3 class="stat__title">MADE FOR DEVELOPERS</h3>
				<div class="stat__text">Lightweight, expertly-coded and open source. Code with our actions and filters.</div>
			</div>
			<div class="stat">
				<div class="stat__icon"><?php rooster_inline_svg( 'lab' ); ?></div>
				<h3 class="stat__title">EXTEND INTO INFINITY</h3>
				<div class="stat__text">Take it to a whole 'nother level with extensions. Get a bundle and save a bundle.</div>
			</div>
		</div>
	</div>
</section>
