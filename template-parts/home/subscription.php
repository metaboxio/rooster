<?php
/**
 * Template part for displaying cta 1 on homepage.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Rooster
 */

?>

<section class="section section--alt subscription">
	<div class="container">
		<h2 class="section__title subscription__heading">Want to learn how to use Meta Box to its full potential?<br>Sign up to get valuable tips and resources.</h2>
		<?php
		$shortcode = '[jetpack_subscription_form title="" subscribe_text=""]';
		echo do_shortcode( $shortcode );
		?>
	</div>
</section>
