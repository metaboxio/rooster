<section class="section section--alt section--center testimonials">
	<div class="container">
		<h2 class="section__title">What Top Developers Are Saying</h2>
		<div class="testimonials__inner grid grid--2">
			<div class="testimonial">
				<img loading="lazy" class="testimonial__image" src="<?= rooster_image_cdn( 'images/ahmad.jpg' ) ?>" width="100" height="100" alt="ahmad awais testimonial">
				<p class="testimonial__text">I have tried many frameworks for building meta boxes. This one is the <strong>best meta box plugin</strong> so far. The developer is pretty active, I have contributed several times. <strong>This plugin stays out of your way and has a pretty neat code base</strong>.</p>
				<div class="testimonial__author"><a href="https://ahmadawais.com/" target="_blank" rel="noopener noreferrer nofollow"><strong>Ahmad Awais</strong></a>, a WordPress core contributor developer</div>
			</div>
			<div class="testimonial">
				<img loading="lazy" class="testimonial__image" src="<?= rooster_image_cdn( 'images/phil.jpg' ) ?>" width="100" height="100" alt="Phil Clothier testimonial">
				<p class="testimonial__text">I much <strong>prefer Meta Box over other similar meta box or custom field frameworks</strong>. They have very good documentation and examples available.</p>
				<div class="testimonial__author"><strong>Phil Clothier</strong>, a Beaver Builder user</div>
			</div>
		</div>
	</div>
</section>
