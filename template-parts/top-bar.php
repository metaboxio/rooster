<?php
/**
 * Template part for displaying top bar
 *
 * @link    https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Rooster
 */

?>

<div class="top-bar">
	<nav class="header-navigation top-bar-navigation">
		<?php
		wp_nav_menu( [
			'theme_location' => 'menu-2',
			'container'      => '',
		] );
		?>
	</nav>
	<nav class="header-navigation top-bar-navigation">
		<?php // get_search_form() ?>
		<ul class="menu">
			<li class="menu-item"><a href="<?= home_url( '/my-account/' ) ?>">My Account</a></li>
			<?php if ( is_user_logged_in() ) : ?><li class="menu-item"><a href="<?= wp_logout_url( home_url() ) ?>">Log Out</a></li><?php endif ?>
		</ul>
	</nav>
</div>
