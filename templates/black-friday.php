<?php
/**
 * Template Name: Black Friday
 */

get_header();
?>
<style>
	.bf-center {
		text-align: center;
	}
	.bf-hero {
		background: url(//i.imgur.com/XkM93Au.jpg) no-repeat center center;
		background-size: cover;
		color: #fff;
	}
	.bf-hero-subtitle1 {
		font-size: 60px;
		letter-spacing: 1px;
		margin-top: 20px;
	}
	.bf-hero-title {
		font-size: 48px;
		margin: 0;
	}
	.bf-hero-subtitle2 {
		margin: 10px 0 40px;
		font-family: inherit;
		text-transform: uppercase;
		font-size: 20px;
		letter-spacing: 1px;
	}
	.bf-time {
		margin-bottom: 10px;
	}
	.countdown {
		list-style: none;
		margin: 0;
		padding: 0;
		display: flex;
		justify-content: center;
		text-align: center;
	}
	.countdown li +li {
		margin-left: 15px;
	}
	.countdown li {
		background: #fff;
		width: 80px;
		border-radius: 4px;
		color: #2c3e50;
	}
	.countdown-number {
		font-size: 40px;
		display: block;
		line-height: 1;
		border-bottom: 1px solid rgba(44, 62, 80, .1);
		padding: 10px 0;
	}
	.countdown-label {
		font-size: 12px;
		display: block;
		text-transform: uppercase;
		padding: 5px 0;
	}
	.cd-2 {
		color: #fff;
		margin-bottom: 50px;
	}
	.cd-2 li {
		background: #2c3e50;
		color: #fff;
	}
	.cd-2 .countdown-number {
		border-bottom-color: rgba(255, 255, 255, .1);
	}
	.bf-badge {
		font-size: 30px;
		margin-top: 40px;
		text-transform: uppercase;
		font-weight: bold;
		letter-spacing: 1px;
	}
	.bf-hero .plan__price--old {
		color: rgba(255,255,255,.6);
	}
	.bf-hero .b-buy {
		margin: 40px 0 30px;
	}
	.intro__arrow a {
		color: #fff;
	}
	.mb-features-grid .pricing__long_desc {
		font-size: 2rem;
		margin-bottom: 60px;
	}
	.mb-features-grid .grid {
		margin-bottom: 30px;
	}
	.bf-cta {
		margin-top: 60px;
	}
	.bf-list {
		list-style: none;
		display: flex;
		margin: 40px auto 0;
		justify-content: space-between;
		text-align: left;
	}
	.bf-list svg {
		fill: #00b1b3;
		width: 14px;
		height: 14px;
		position: relative;
		top: 2px;
	}
	.bf-pricing-desc {
		text-transform: uppercase;
		letter-spacing: 1px;
		margin-bottom: 10px;
		font-weight: bold;
	}
	.bf-pricing-desc + .section__title {
		margin-top: 0;
	}
	@media (max-width: 767px) {
		.section {
			padding: 40px 0;
		}
		.section__title {
			font-size: 28px;
		}
		.mb-features-grid .grid h3 {
			font-size: 22px;
		}
		.mb-feature {
			padding-top: 30px;
		}
		.bf-hero-title {
			font-size: 40px;
		}
		.stat__title {
			margin-top: 10px;
			margin-bottom: 0;
		}
		.bf-image {
			text-align: center;
			order: 1;
		}
		.bf-body {
			order: 2;
		}
		.bf-list {
			flex-direction: column;
		}
		.plans {
			margin: 40px 0;
		}
		.plan--popular {
			padding-left: 25px;
			padding-right: 25px;
		}
	}
</style>
<main id="main" class="site-main" role="main">

	<section class="section section--center bf-hero">
		<div class="container">
			<div class="bf-hero-subtitle1">60% OFF</div>
			<h1 class="bf-hero-title">Meta Box's Lifetime Bundle</h1>
			<h2 class="bf-hero-subtitle2">Black Friday and Cyber Monday 2019</h2>
			<div class="bf-time">Limited Time Only: Nov 26 - Dec 06</div>

			<ul class="countdown" data-end="2019-12-06T23:59:59+00:00">
				<li><span class="countdown-number countdown-days">0</span><span class="countdown-label">Days</span></li>
				<li><span class="countdown-number countdown-hours">0</span><span class="countdown-label">Hours</span></li>
				<li><span class="countdown-number countdown-minutes">0</span><span class="countdown-label">Minutes</span></li>
				<li><span class="countdown-number countdown-seconds">0</span><span class="countdown-label">Seconds</span></li>
			</ul>

			<div class="bf-badge">Best Deal Ever!</div>
			<div class="plan__price"><sup>$</sup>199</div>
			<div class="plan__price--old">was $499</div>
			<?= do_shortcode( '[buy_button id="6174" class="button" gumroad="//gum.co/mb-lifetime-bundle/bf2019" coupon="BF2019" text="Get the deal now"]' ); ?>

			<div class="intro__arrow"><a href="#bf-leading" class="jump"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129"><path d="m121.3,34.6c-1.6-1.6-4.2-1.6-5.8,0l-51,51.1-51.1-51.1c-1.6-1.6-4.2-1.6-5.8,0-1.6,1.6-1.6,4.2 0,5.8l53.9,53.9c0.8,0.8 1.8,1.2 2.9,1.2 1,0 2.1-0.4 2.9-1.2l53.9-53.9c1.7-1.6 1.7-4.2 0.1-5.8z"></path></svg></a></div>
		</div>
	</section>

	<section class="section section--center" id="bf-leading">
		<div class="container">
			<h2 class="section__title">What do you get?</h2>
			<div class="grid grid--4">
				<div class="stat">
					<div class="stat__icon"><svg aria-hidden="true" focusable="false" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M104 224H24c-13.255 0-24 10.745-24 24v240c0 13.255 10.745 24 24 24h80c13.255 0 24-10.745 24-24V248c0-13.255-10.745-24-24-24zM64 472c-13.255 0-24-10.745-24-24s10.745-24 24-24 24 10.745 24 24-10.745 24-24 24zM384 81.452c0 42.416-25.97 66.208-33.277 94.548h101.723c33.397 0 59.397 27.746 59.553 58.098.084 17.938-7.546 37.249-19.439 49.197l-.11.11c9.836 23.337 8.237 56.037-9.308 79.469 8.681 25.895-.069 57.704-16.382 74.757 4.298 17.598 2.244 32.575-6.148 44.632C440.202 511.587 389.616 512 346.839 512l-2.845-.001c-48.287-.017-87.806-17.598-119.56-31.725-15.957-7.099-36.821-15.887-52.651-16.178-6.54-.12-11.783-5.457-11.783-11.998v-213.77c0-3.2 1.282-6.271 3.558-8.521 39.614-39.144 56.648-80.587 89.117-113.111 14.804-14.832 20.188-37.236 25.393-58.902C282.515 39.293 291.817 0 312 0c24 0 72 8 72 81.452z"></path></svg></div>
					<h3 class="stat__title">Industry Leading<br>Custom Fields Plugin</h3>
				</div>
				<div class="stat">
					<div class="stat__icon"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path d="M8 6.02V3c0-1.654-1.346-3-3-3S2 1.346 2 3v3.02C.792 6.937 0 8.37 0 10s.792 3.064 2 3.977V29c0 1.654 1.346 3 3 3s3-1.346 3-3V13.977c1.208-.912 2-2.346 2-3.977s-.792-3.064-2-3.98zM4 3c0-.553.447-1 1-1s1 .447 1 1v2.1c-.323-.065-.657-.1-1-.1s-.677.035-1 .1V3zm2 26c0 .553-.447 1-1 1s-1-.447-1-1V14.898c.323.067.657.102 1 .102s.677-.035 1-.102V29zm1.865-18.16c-.016.053-.03.105-.05.158-.094.264-.216.514-.377.736l-.014.016c-.174.238-.38.45-.616.627l-.01.008c-.24.182-.51.328-.8.43-.312.113-.645.185-.998.185s-.686-.072-1-.186c-.29-.102-.558-.248-.8-.43l-.01-.008c-.234-.178-.44-.39-.615-.627-.004-.007-.01-.01-.014-.017-.16-.223-.282-.473-.377-.736-.02-.053-.033-.105-.05-.158C2.056 10.57 2 10.292 2 10c0-.295.055-.574.135-.842.016-.053.03-.105.05-.156.093-.264.215-.514.377-.738l.014-.016c.174-.236.38-.45.616-.627.004 0 .007-.005.01-.007.24-.18.51-.326.798-.43.314-.11.647-.183 1-.183s.686.072 1 .184c.29.104.558.25.8.43.002.002.005.006.01.008.234.178.44.39.615.627l.014.015c.16.223.282.473.376.737.02.05.034.103.05.156.08.268.134.547.134.842 0 .293-.055.572-.135.84zM30 6.02V3c0-1.654-1.346-3-3-3s-3 1.346-3 3v3.02c-1.21.916-2 2.35-2 3.98s.79 3.064 2 3.977V29c0 1.654 1.346 3 3 3s3-1.346 3-3V13.977c1.207-.912 2-2.346 2-3.977s-.793-3.064-2-3.98zM26 3c0-.553.447-1 1-1s1 .447 1 1v2.1c-.324-.065-.658-.1-1-.1-.344 0-.678.035-1 .1V3zm2 26c0 .553-.447 1-1 1s-1-.447-1-1V14.898c.322.067.656.102 1 .102.342 0 .676-.035 1-.102V29zm1.865-18.16c-.016.053-.03.105-.05.158-.095.264-.216.514-.378.736l-.014.016c-.174.238-.38.45-.615.627l-.01.008c-.242.182-.51.328-.8.43-.312.113-.646.185-.998.185-.354 0-.686-.072-1-.186-.29-.102-.56-.248-.8-.43l-.01-.008c-.235-.178-.442-.39-.616-.627-.004-.007-.01-.01-.014-.017-.16-.223-.283-.473-.377-.736-.02-.053-.033-.105-.05-.158-.078-.268-.133-.547-.133-.84 0-.295.055-.574.135-.842.016-.053.03-.105.05-.156.093-.264.216-.514.376-.738l.015-.016c.174-.236.38-.45.617-.627.004 0 .006-.005.01-.007.24-.18.51-.326.8-.43.312-.11.644-.183.998-.183.352 0 .686.072 1 .184.29.104.557.25.8.43 0 .002.005.006.01.008.233.178.44.39.614.627l.014.015c.162.225.283.475.38.738.017.05.032.104.048.156.08.266.134.545.134.84 0 .293-.055.572-.135.84zM19 18.02V3c0-1.654-1.346-3-3-3s-3 1.346-3 3v15.02c-1.208.915-2 2.35-2 3.98s.792 3.064 2 3.977V29c0 1.654 1.346 3 3 3s3-1.346 3-3v-3.023c1.207-.912 2-2.346 2-3.977s-.793-3.064-2-3.98zM15 3c0-.553.447-1 1-1s1 .447 1 1v14.1c-.324-.064-.658-.1-1-.1-.343 0-.677.035-1 .1V3zm2 26c0 .553-.447 1-1 1s-1-.447-1-1v-2.102c.323.067.657.102 1 .102.342 0 .676-.035 1-.102V29zm1.865-6.16c-.016.053-.03.105-.05.158-.095.264-.216.514-.378.736l-.014.016c-.174.238-.38.45-.615.627l-.01.008c-.242.182-.51.328-.8.43-.312.113-.646.185-.998.185s-.686-.072-1-.186c-.29-.102-.558-.248-.8-.43l-.01-.008c-.234-.178-.44-.39-.615-.627-.004-.007-.01-.01-.014-.017-.16-.223-.282-.473-.377-.736-.02-.053-.033-.105-.05-.158-.078-.268-.133-.547-.133-.84 0-.295.055-.574.135-.842.016-.053.03-.105.05-.156.094-.264.216-.514.377-.738l.014-.016c.174-.236.38-.45.616-.627.004 0 .007-.005.01-.007.24-.18.51-.326.8-.43.312-.11.645-.183.998-.183.352 0 .686.072 1 .184.29.104.557.25.8.43 0 .002.005.006.01.008.233.178.44.39.614.627l.014.015c.162.225.283.475.38.738.017.05.032.104.048.156.08.266.134.545.134.84 0 .293-.055.572-.135.84z"></path></svg></div>
					<h3 class="stat__title">Top-Notch Features</h3>
				</div>
				<div class="stat">
					<div class="stat__icon"><svg aria-hidden="true" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><path d="M216 288h-48c-8.84 0-16 7.16-16 16v192c0 8.84 7.16 16 16 16h48c8.84 0 16-7.16 16-16V304c0-8.84-7.16-16-16-16zM88 384H40c-8.84 0-16 7.16-16 16v96c0 8.84 7.16 16 16 16h48c8.84 0 16-7.16 16-16v-96c0-8.84-7.16-16-16-16zm256-192h-48c-8.84 0-16 7.16-16 16v288c0 8.84 7.16 16 16 16h48c8.84 0 16-7.16 16-16V208c0-8.84-7.16-16-16-16zm128-96h-48c-8.84 0-16 7.16-16 16v384c0 8.84 7.16 16 16 16h48c8.84 0 16-7.16 16-16V112c0-8.84-7.16-16-16-16zM600 0h-48c-8.84 0-16 7.16-16 16v480c0 8.84 7.16 16 16 16h48c8.84 0 16-7.16 16-16V16c0-8.84-7.16-16-16-16z"></path></svg></div>
					<h3 class="stat__title">Unlimited Domain Usage</h3>
				</div>
				<div class="stat">
					<div class="stat__icon"><svg aria-hidden="true" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M0 252.118V48C0 21.49 21.49 0 48 0h204.118a48 48 0 0 1 33.941 14.059l211.882 211.882c18.745 18.745 18.745 49.137 0 67.882L293.823 497.941c-18.745 18.745-49.137 18.745-67.882 0L14.059 286.059A48 48 0 0 1 0 252.118zM112 64c-26.51 0-48 21.49-48 48s21.49 48 48 48 48-21.49 48-48-21.49-48-48-48z"></path></svg></div>
					<h3 class="stat__title">One-time Payment<br>Lifetime Usage</h3>
				</div>
			</div>
		</div>
	</section>

	<section class="section section--alt">
		<div class="container">
			<h2 class="section__title bf-center">Top 6 Reasons Why People Love Meta Box</h2>
			<p class="section__description pricing__long_desc bf-center">With more than 400,000 active installs, Meta Box is the best choice for developers, who wants a lightweight yet powerful plugin to control data in WordPress.</p>

			<div class="mb-features">
				<div class="mb-feature">
					<div class="mb-feature__image">
						<svg class="icon icon-paperplane" aria-hidden="true" role="img">
							<use href="#icon-paperplane" xlink:href="#icon-paperplane"></use>
						</svg>
					</div>
					<div class="mb-feature__body">
						<h3>Powerful yet weightless</h3>
						<p>This custom fields plugin for WordPress is speedy and supercharged with no complicated plugin options or admin pages—only quickly integratable code.</p>
					</div>
				</div>

				<div class="mb-feature">
					<div class="mb-feature__image">
						<svg class="icon icon-params" aria-hidden="true" role="img">
							<use href="#icon-params" xlink:href="#icon-params"></use>
						</svg>
					</div>
					<div class="mb-feature__body">
						<h3>Create advanced fields the easiest ways</h3>
						<p>Adding custom fields to WordPress doesn't have to be difficult or time-consuming. Meta Box works smoothly with any theme, plugin or website.</p>
					</div>
				</div>
				<div class="mb-feature">
					<div class="mb-feature__image">
						<svg class="icon icon-note" aria-hidden="true" role="img">
							<use href="#icon-note" xlink:href="#icon-note"></use>
						</svg>
					</div>
					<div class="mb-feature__body">
						<h3>Support 40+ field types</h3>
						<p>Meta Box supports 40+ field types for your full customization needs. Feeling creative? You can also effortlessly create your own field types for any WordPress custom meta box.</p>
					</div>
				</div>
				<div class="mb-feature">
					<div class="mb-feature__image">
						<svg class="icon icon-note" aria-hidden="true" role="img">
							<use href="#icon-note" xlink:href="#icon-note"></use>
						</svg>
					</div>
					<div class="mb-feature__body">
						<h3>Drag &amp; Drop UI</h3>
						<p>Don't want to touch code? No problem. Our friendly UI builder helps you build custom post types, custom taxonomies, meta boxes and custom fields in just a few minutes.</p>
					</div>
				</div>
				<div class="mb-feature">
					<div class="mb-feature__image">
						<svg class="icon icon-note" aria-hidden="true" role="img">
							<use href="#icon-note" xlink:href="#icon-note"></use>
						</svg>
					</div>
					<div class="mb-feature__body">
						<h3>Compatible with 3rd plugins &amp; page builders</h3>
						<p>Meta Box works perfectly with popular plugins and page builders, which allow you to create complex websites with ease.</p>
					</div>
				</div>
				<div class="mb-feature">
					<div class="mb-feature__image">
						<svg class="icon icon-copy" aria-hidden="true" role="img">
							<use href="#icon-copy" xlink:href="#icon-copy"></use>
						</svg>
					</div>
					<div class="mb-feature__body">
						<h3>Multisite compatible</h3>
						<p>Meta Box is fully compatible and works well with both individual and Multisite installations.</p>
					</div>
				</div>
			</div>

			<div class="bf-center bf-cta">
				<?= do_shortcode( '[buy_button id="6174" class="button" gumroad="//gum.co/mb-lifetime-bundle/bf2019" coupon="BF2019" text="Get the deal now"]' ); ?>
			</div>
		</div>
	</section>

	<section class="section mb-features-grid">
		<div class="container">
			<h2 class="section__title bf-center">Top-Notch Features</h2>
			<p class="section__description pricing__long_desc bf-center">Meta Box is a must-have toolkit for WordPress developers. Here are the features that make Meta Box the most powerful WordPress custom field plugin.</p>
			<div class="grid grid--2 grid--center">
				<div class="bf-body">
					<h3>Just Drag and Drop It</h3>
					<p>Intuitive interface to create and configure custom fields, custom post types, and taxonomies.</p>
					<p>Export and import easily to clone from site to site without the extra weight of the extension.</p>
				</div>
				<div class="bf-image">
					<img src="//i.imgur.com/xZJxeTF.png" alt="wordpress custom fields builder" width="555" height="400">
				</div>
			</div>
			<div class="grid grid--2 grid--center">
				<div class="bf-image">
					<img src="//i.imgur.com/BXmyIBx.png" alt="custom fields repeater field, group field" width="555" height="400">
				</div>
				<div class="bf-body">
					<h3>Box It Up</h3>
					<p>Organize your fields and sub-fields into manageable and cloneable blocks.</p>
					<p>Supports multi- and unlimited-level nesting. You can also duplicate any group with ease.</p>
				</div>
			</div>
			<div class="grid grid--2 grid--center">
				<div class="bf-body">
					<h3>Bring It To The Frontend Easily</h3>
					<p>Need a custom form for users to can submit content from the frontend? It's easier than ever with the shortcode.</p>
					<p>Bring all the custom fields to the frontend, even the premium features such as conditional logic, columns and tabs.</p>
				</div>
				<div class="bf-image">
					<img src="//i.imgur.com/xbahDuy.png" alt="user submitted posts" width="555" height="400">
				</div>
			</div>
			<div class="grid grid--2 grid--center">
				<div class="bf-image">
					<img src="//i.imgur.com/KSGKFF5.png" alt="save custom fields in custom table" width="555" height="400">
				</div>
				<div class="bf-body">
					<h3>Custom Table Storage</h3>
					<p>The WordPress meta table for storing metadata is super flexible but also limited in terms of specifics.</p>
					<p>Organize your collected data efficiently and per your workflow so you can easily access anything you need from any WordPress custom meta box.</p>
				</div>
			</div>
			<div class="grid grid--2 grid--center">
				<div class="bf-body">
					<h3>Build Your Own Gutenberg Blocks</h3>
					<p>Faster create custom Gutenberg blocks for specific pieces of content.</p>
					<p>No more React, no learning-curve. It's just PHP and a simple array.</p>
				</div>
				<div class="bf-image">
					<img src="//i.imgur.com/2LnwaCt.png" alt="save custom fields in custom table" width="555" height="400">
				</div>
			</div>
		</div>
	</section>

	<section class="section section--alt section--center">
		<div class="container">
			<h2 class="section__title bf-center">And More Features</h2>
			<p class="section__description pricing__long_desc bf-center">You own all the current premium features and even the upcoming ones which provide you endless possibilities.</p>
			<ul class="bf-list">
				<li>
					<svg class="icon"><use xlink:href="#checkmark-outline" /></svg> Create relationships<br>
					<svg class="icon"><use xlink:href="#checkmark-outline" /></svg> Create custom settings pages
				</li>
				<li>
					<svg class="icon"><use xlink:href="#checkmark-outline" /></svg> Add custom fields to user profile<br>
					<svg class="icon"><use xlink:href="#checkmark-outline" /></svg> Save custom fields to custom tables
				</li>
				<li>
					<svg class="icon"><use xlink:href="#checkmark-outline" /></svg> Track changes of custom fields<br>
					<svg class="icon"><use xlink:href="#checkmark-outline" /></svg> Populate location data with Google API
				</li>
				<li>
					<svg class="icon"><use xlink:href="#checkmark-outline" /></svg> Display help information with tooltips<br>
					<svg class="icon"><use xlink:href="#checkmark-outline" /></svg> And more...
				</li>
			</ul>
			<div class="bf-center bf-cta">
				<?= do_shortcode( '[buy_button id="6174" class="button" gumroad="//gum.co/mb-lifetime-bundle/bf2019" coupon="BF2019" text="Get the deal now"]' ); ?>
			</div>
		</div>
	</section>

	<section class="section section--center">
		<div class="container">
			<p class="bf-pricing-desc">One-Time Payment &rarr; Lifetime Usage + Unlimited Domains</p>
			<h2 class="section__title">Save $300 Now!</h2>
			<p class="section__description pricing__long_desc">Difference between Lifetime Bundle vs others</p>
			<div class="plans" id="plans">
				<div class="plan">
					<h3 class="plan__title">Core Bundle</h3>
					<div class="plan__price"><sup>$</sup>79</div>
					<div class="plan__description">Essential plugins for custom fields</div>
					<ul class="plan__features">
						<li class="plan__feature">
							<svg class="icon"><use xlink:href="#checkmark-outline" /></svg>
							<strong>12 PRO extensions</strong>
						</li>
						<li class="plan__feature">
							<svg class="icon"><use xlink:href="#checkmark-outline" /></svg>
							Composer support
						</li>
						<li class="plan__feature">
							<svg class="icon"><use xlink:href="#checkmark-outline" /></svg>
							<strong>1 year of updates</strong>
						</li>
						<li class="plan__feature">
							<svg class="icon"><use xlink:href="#checkmark-outline" /></svg>
							<strong>1 year of support</strong>
						</li>
						<li class="plan__feature">
							<svg class="icon"><use xlink:href="#checkmark-outline" /></svg>
							Unlimited sites
						</li>
						<li class="plan__feature">
							<svg class="icon"><use xlink:href="#checkmark-outline" /></svg>
							Upgrade anytime
						</li>
					</ul>
				</div>
				<div class="plan plan--popular">
					<div class="plan__badge">LIMITED-TIME ONLY</div>
					<h3 class="plan__title">Lifetime Bundle</h3>
					<div class="plan__price"><sup>$</sup>199</div>
					<div class="plan__price--old">was <sup>$</sup><s>499</s></div>
					<div class="plan__description">Lifetime VIP plan for professional users</div>
					<div class="plan__description" style="color: #d91e18; font-style: normal; font-weight: bold">Coupon BF2019 - 60% OFF<br>Valid until Dec 06</div>
					<ul class="plan__features">
						<li class="plan__feature">
							<svg class="icon"><use xlink:href="#checkmark-outline" /></svg>
							<strong>All PRO extensions</strong>
						</li>
						<li class="plan__feature">
							<svg class="icon"><use xlink:href="#checkmark-outline" /></svg>
							<strong>All future PRO extensions</strong>
						</li>
						<li class="plan__feature">
							<svg class="icon"><use xlink:href="#checkmark-outline" /></svg>
							Meta Box All-In-One
						</li>
						<li class="plan__feature">
							<svg class="icon"><use xlink:href="#checkmark-outline" /></svg>
							Composer support
						</li>
						<li class="plan__feature">
							<svg class="icon"><use xlink:href="#checkmark-outline" /></svg>
							<strong>Lifetime updates</strong>
						</li>
						<li class="plan__feature">
							<svg class="icon"><use xlink:href="#checkmark-outline" /></svg>
							<strong>Lifetime support</strong>
						</li>
						<li class="plan__feature">
							<svg class="icon"><use xlink:href="#checkmark-outline" /></svg>
							Unlimited sites
						</li>
					</ul>
					<div class="plan__cta">
						<?= do_shortcode( '[buy_button class="plan__cta__button button" id="6174" coupon="BF2019" gumroad="//gum.co/mb-lifetime-bundle/bf2019"]' ) ?>
					</div>
				</div>
				<div class="plan">
					<h3 class="plan__title">Developer Bundle</h3>
					<div class="plan__price"><sup>$</sup>149</div>
					<div class="plan__description">Everything you need for custom fields</div>
					<ul class="plan__features">
						<li class="plan__feature">
							<svg class="icon"><use xlink:href="#checkmark-outline" /></svg>
							<strong>All PRO extensions</strong>
						</li>
						<li class="plan__feature">
							<svg class="icon"><use xlink:href="#checkmark-outline" /></svg>
							Meta Box All-In-One
						</li>
						<li class="plan__feature">
							<svg class="icon"><use xlink:href="#checkmark-outline" /></svg>
							Composer support
						</li>
						<li class="plan__feature">
							<svg class="icon"><use xlink:href="#checkmark-outline" /></svg>
							<strong>1 year of updates</strong>
						</li>
						<li class="plan__feature">
							<svg class="icon"><use xlink:href="#checkmark-outline" /></svg>
							<strong>1 year of support</strong>
						</li>
						<li class="plan__feature">
							<svg class="icon"><use xlink:href="#checkmark-outline" /></svg>
							Unlimited sites
						</li>
						<li class="plan__feature">
							<svg class="icon"><use xlink:href="#checkmark-outline" /></svg>
							Upgrade anytime
						</li>
					</ul>
				</div>
			</div>

			<ul class="countdown cd-2" data-end="2019-12-06T23:59:59+00:00">
				<li><span class="countdown-number countdown-days">0</span><span class="countdown-label">Days</span></li>
				<li><span class="countdown-number countdown-hours">0</span><span class="countdown-label">Hours</span></li>
				<li><span class="countdown-number countdown-minutes">0</span><span class="countdown-label">Minutes</span></li>
				<li><span class="countdown-number countdown-seconds">0</span><span class="countdown-label">Seconds</span></li>
			</ul>

			<p><strong>100% Satisfaction Guaranteed</strong> — 14-day refund policy.</p>
			<p><img src="//i0.wp.com/gretathemes.com/wp-content/uploads/2019/03/payment-gateways.png" alt="payment gateways" width="358" height="110"></p>
		</div>
	</section>

	<section class="section section--alt section--center testimonials">
		<div class="container">
			<h2 class="section__title">What Top Developers Are Saying</h2>
			<div class="testimonials__inner grid grid--2">
				<div class="testimonial">
					<img class="testimonial__image" src="//i0.wp.com/metabox.io/wp-content/themes/rooster/images/ahmad.jpg" width="100" height="100" alt="ahmad awais testimonial">
					<p class="testimonial__text">I have tried many frameworks for building meta boxes. This one is the <strong>best meta box plugin</strong> so far. The developer is pretty active, I have contributed several times. <strong>This plugin stays out of your way and has a pretty neat code base</strong>.</p>
					<div class="testimonial__author"><strong>Ahmad Awais</strong>, a WordPress core contributor developer</div>
				</div>
				<div class="testimonial">
					<img class="testimonial__image" src="//i0.wp.com/metabox.io/wp-content/themes/rooster/images/phil.jpg" width="100" height="100" alt="Phil Clothier testimonial">
					<p class="testimonial__text">I much <strong>prefer Meta Box over other similar meta box or custom field frameworks</strong>. They have very good documentation and examples available.</p>
					<div class="testimonial__author"><strong>Phil Clothier</strong>, a Beaver Builder user</div>
				</div>
			</div>
		</div>
	</section>

	<section class="section">
		<div class="pricing__faq container">
			<h2 class="section__title">Frequently Asked Questions</h2>
			<div class="pricing__questions">
				<div class="pricing__question">
					<h3>Who can use Meta Box?</h3>
					<p>WordPress site creators who want to save loads of time building custom fields and custom meta boxes. This WordPress custom plugin is a library and framework that helps you create quickly and precisely. We have options for people familiar with code and also for those who don't even want to look at it.</p>
				</div>
				<div class="pricing__question">
					<h3>Is Meta Box FREE?</h3>
					<p>Yes, the core Meta Box plugin is completely free. If you need more features, consider the optional extensions—either free or premium.</p>
				</div>
				<div class="pricing__question">
					<h3>Will Meta Box slow down my website?</h3>
					<p>Absolutely not. Meta Box is carefully built with performance in mind. It loads only the needed modules when required and excels even on high-traffic websites.</p>
				</div>
				<div class="pricing__question">
					<h3>Can I use Meta Box within a theme I sell on Themeforest.net?</h3>
					<p>Yes! Meta Box is free and you can use the plugin and extensions to build premium themes or WordPress custom plugins and sell them on your own website or in marketplaces like Themeforest.</p>
				</div>
				<div class="pricing__question">
					<h3>Can I use Meta Box on client sites?</h3>
					<p>Yes, absolutely. You can use the plugin and extensions on as many client sites as you want. It will save you a ton of time and allow you to focus on other important aspects of the website.</p>
				</div>
				<div class="pricing__question">
					<h3>How can I get the download of the extensions?</h3>
					<p>You will receive an instant download after purchasing. The download link is also sent to your email. The core Meta Box plugin can also be downloaded from the WordPress.org plugin repository.</p>
				</div>
				<div class="pricing__question">
					<h3>What is the software license of your extensions?</h3>
					<p>GNU GPL v2+. You can use extensions in your free and commercial products. The only restriction is you cannot include premium extensions in a free product or share them publicly.</p>
				</div>
				<div class="pricing__question">
					<h3>What's your refund policy?</h3>
					<p>You are more than welcome to request a refund within 14 days of purchasing extensions for your WordPress custom plugin. If Meta Box doesn't do everything we've promised, we will happily refund 100% of your money.</p>
				</div>
			</div>

			<div class="bf-center bf-cta">
				<?= do_shortcode( '[buy_button id="6174" class="button" gumroad="//gum.co/mb-lifetime-bundle/bf2019" coupon="BF2019" text="Get the deal now"]' ); ?>
			</div>
		</div>
	</section>

	<svg style="position: absolute; width: 0; height: 0; overflow: hidden;" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		<symbol id="icon-import_export" viewBox="0 0 24 24">
			<path d="M15.984 17.016h3l-3.984 3.984-3.984-3.984h3v-7.031h1.969v7.031zM9 3l3.984 3.984h-3v7.031h-1.969v-7.031h-3z"></path>
		</symbol>
		<symbol id="icon-heart" viewBox="0 0 32 32">
			<path d="M29.193 5.265c-3.629-3.596-9.432-3.671-13.191-0.288-3.76-3.383-9.561-3.308-13.192 0.288-3.741 3.704-3.741 9.709 0 13.415 1.069 1.059 11.053 10.941 11.053 10.941 1.183 1.172 3.096 1.172 4.278 0 0 0 10.932-10.822 11.053-10.941 3.742-3.706 3.742-9.711-0.001-13.415zM27.768 17.268l-11.053 10.941c-0.393 0.391-1.034 0.391-1.425 0l-11.053-10.941c-2.95-2.92-2.95-7.671 0-10.591 2.844-2.815 7.416-2.914 10.409-0.222l1.356 1.22 1.355-1.22c2.994-2.692 7.566-2.594 10.41 0.222 2.95 2.919 2.95 7.67 0.001 10.591zM9.253 7.501c0.277 0 0.5 0.224 0.5 0.5s-0.224 0.5-0.5 0.5h-0.001c-1.794 0-3.249 1.455-3.249 3.249v0.001c0 0.276-0.224 0.5-0.5 0.5s-0.5-0.224-0.5-0.5v0c0-2.346 1.901-4.247 4.246-4.249 0.002 0 0.002-0.001 0.004-0.001z"></path>
		</symbol>
		<symbol id="icon-star" viewBox="0 0 32 32">
			<path d="M31.881 12.557c-0.277-0.799-0.988-1.384-1.844-1.511l-8.326-1.238-3.619-7.514c-0.381-0.789-1.196-1.294-2.092-1.294s-1.711 0.505-2.092 1.294l-3.619 7.514-8.327 1.238c-0.855 0.127-1.566 0.712-1.842 1.511-0.275 0.801-0.067 1.683 0.537 2.285l6.102 6.092-1.415 8.451c-0.144 0.851 0.225 1.705 0.948 2.203 0.397 0.273 0.864 0.412 1.331 0.412 0.384 0 0.769-0.094 1.118-0.281l7.259-3.908 7.26 3.908c0.349 0.187 0.734 0.281 1.117 0.281 0.467 0 0.934-0.139 1.332-0.412 0.723-0.498 1.090-1.352 0.947-2.203l-1.416-8.451 6.104-6.092c0.603-0.603 0.81-1.485 0.537-2.285zM23.588 19.363c-0.512 0.51-0.744 1.229-0.627 1.934l1.416 8.451-7.26-3.906c-0.348-0.188-0.732-0.281-1.118-0.281-0.384 0-0.769 0.094-1.117 0.281l-7.26 3.906 1.416-8.451c0.118-0.705-0.114-1.424-0.626-1.934l-6.102-6.092 8.326-1.24c0.761-0.113 1.416-0.589 1.743-1.268l3.621-7.512 3.62 7.513c0.328 0.679 0.982 1.154 1.742 1.268l8.328 1.24-6.102 6.091z"></path>
		</symbol>
		<symbol id="icon-user" viewBox="0 0 32 32">
			<path d="M31.11 28.336c-0.201-0.133-3.848-2.525-9.273-3.699 1.99-2.521 3.268-5.912 3.811-8.169 0.754-3.128 0.461-9.248-2.543-13.062-1.756-2.229-4.213-3.406-7.105-3.406s-5.349 1.178-7.104 3.406c-3.004 3.813-3.296 9.933-2.543 13.061 0.543 2.257 1.82 5.648 3.811 8.169-5.425 1.174-9.072 3.566-9.272 3.699-0.733 0.488-1.061 1.4-0.805 2.242 0.254 0.845 1.033 1.423 1.913 1.423h28c0.881 0 1.658-0.578 1.914-1.422 0.257-0.842-0.071-1.754-0.804-2.242zM20.267 23.398l-0.326 0.414c-2.385 2.74-5.495 2.74-7.879 0l-0.327-0.414c-2.785-3.529-4.167-8.197-3.572-12.65 0.545-4.279 2.997-8.748 7.837-8.748s7.293 4.47 7.838 8.749c0.593 4.455-0.784 9.118-3.571 12.649zM2 30c0.138-0.092 3.526-2.314 8.586-3.408l2.484-0.537c0.887 0.582 1.86 0.945 2.93 0.945 1.071 0 2.043-0.363 2.93-0.945l2.484 0.537c5.020 1.086 8.396 3.283 8.586 3.408h-28z"></path>
		</symbol>
		<symbol id="icon-search" viewBox="0 0 32 32">
			<path d="M20 0.005c-6.627 0-12 5.373-12 12 0 2.026 0.507 3.933 1.395 5.608l-8.344 8.342 0.007 0.006c-0.652 0.641-1.058 1.529-1.058 2.516 0 1.949 1.58 3.529 3.529 3.529 0.985 0 1.874-0.406 2.515-1.059l-0.002-0.002 8.341-8.34c1.676 0.891 3.586 1.4 5.617 1.4 6.627 0 12-5.373 12-12s-5.373-12-12-12zM4.795 29.697c-0.322 0.334-0.768 0.543-1.266 0.543-0.975 0-1.765-0.789-1.765-1.764 0-0.498 0.21-0.943 0.543-1.266l-0.009-0.008 8.066-8.066c0.705 0.951 1.545 1.791 2.494 2.498l-8.063 8.063zM20 22.006c-5.522 0-10-4.479-10-10s4.478-10 10-10c5.521 0 10 4.478 10 10s-4.479 10-10 10zM20 5.005c0.275 0 0.5 0.224 0.5 0.5s-0.225 0.5-0.5 0.5c-3.314 0-6 2.687-6 6 0 0.276-0.224 0.5-0.5 0.5s-0.5-0.224-0.5-0.5c0-3.866 3.133-7 7-7z"></path>
		</symbol>
		<symbol id="icon-settings" viewBox="0 0 32 32">
			<path d="M30.391 12.68l-3.064-0.614c-0.154-0.443-0.336-0.873-0.537-1.289l1.736-2.604c0.529-0.793 0.424-1.85-0.25-2.523l-1.924-1.924c-0.387-0.387-0.898-0.586-1.416-0.586-0.383 0-0.77 0.11-1.107 0.336l-2.604 1.735c-0.418-0.202-0.848-0.382-1.291-0.536l-0.614-3.065c-0.186-0.936-1.008-1.608-1.961-1.608h-2.72c-0.953 0-1.774 0.673-1.961 1.608l-0.614 3.065c-0.443 0.154-0.873 0.335-1.289 0.536l-2.603-1.735c-0.339-0.226-0.725-0.336-1.109-0.336-0.517 0-1.028 0.199-1.415 0.586l-1.923 1.924c-0.674 0.674-0.779 1.73-0.25 2.523l1.735 2.604c-0.202 0.417-0.382 0.847-0.536 1.29l-3.066 0.613c-0.935 0.187-1.608 1.008-1.608 1.961v2.72c0 0.953 0.673 1.775 1.608 1.961l3.065 0.615c0.154 0.443 0.335 0.873 0.536 1.289l-1.734 2.604c-0.529 0.793-0.424 1.85 0.25 2.523l1.924 1.924c0.387 0.387 0.898 0.586 1.415 0.586 0.384 0 0.771-0.111 1.108-0.336l2.604-1.736c0.417 0.203 0.847 0.383 1.29 0.537l0.613 3.064c0.187 0.936 1.008 1.609 1.961 1.609h2.72c0.953 0 1.775-0.674 1.961-1.609l0.615-3.064c0.443-0.154 0.873-0.336 1.289-0.537l2.604 1.736c0.338 0.225 0.725 0.336 1.107 0.336 0.518 0 1.029-0.199 1.416-0.586l1.924-1.924c0.674-0.674 0.779-1.73 0.25-2.523l-1.736-2.604c0.203-0.418 0.383-0.848 0.537-1.291l3.064-0.613c0.935-0.185 1.609-1.008 1.609-1.961v-2.72c0-0.953-0.674-1.774-1.609-1.961zM26.934 17.975c-0.695 0.139-1.264 0.635-1.496 1.305-0.129 0.369-0.279 0.727-0.447 1.074-0.311 0.639-0.258 1.393 0.135 1.982l1.736 2.604-1.924 1.924-2.604-1.736c-0.334-0.223-0.721-0.336-1.109-0.336-0.297 0-0.596 0.066-0.871 0.199-0.348 0.168-0.705 0.32-1.076 0.449-0.668 0.232-1.164 0.801-1.303 1.496l-0.615 3.066h-2.72l-0.613-3.066c-0.139-0.695-0.635-1.264-1.304-1.496-0.369-0.129-0.728-0.279-1.075-0.447-0.276-0.135-0.574-0.201-0.872-0.201-0.389 0-0.775 0.113-1.109 0.336l-2.604 1.736-1.924-1.924 1.735-2.604c0.393-0.59 0.444-1.344 0.137-1.98-0.168-0.348-0.319-0.705-0.448-1.076-0.232-0.668-0.802-1.164-1.496-1.303l-3.065-0.615-0.002-2.721 3.066-0.613c0.694-0.139 1.264-0.635 1.496-1.304 0.129-0.369 0.278-0.728 0.447-1.075 0.31-0.638 0.258-1.392-0.136-1.981l-1.734-2.604 1.923-1.924 2.604 1.735c0.334 0.223 0.721 0.336 1.109 0.336 0.297 0 0.595-0.066 0.871-0.199 0.347-0.168 0.705-0.319 1.075-0.448 0.669-0.232 1.165-0.802 1.304-1.496l0.614-3.065 2.72-0.001 0.613 3.066c0.139 0.694 0.635 1.264 1.305 1.496 0.369 0.129 0.727 0.278 1.074 0.447 0.277 0.134 0.574 0.2 0.873 0.2 0.389 0 0.775-0.113 1.109-0.336l2.604-1.735 1.924 1.924-1.736 2.604c-0.393 0.59-0.443 1.343-0.137 1.98 0.168 0.347 0.32 0.705 0.449 1.075 0.232 0.669 0.801 1.165 1.496 1.304l3.064 0.614 0.003 2.72-3.066 0.614zM16 9.001c-3.865 0-7 3.135-7 7 0 3.866 3.135 7 7 7s7-3.135 7-7c0-3.865-3.135-7-7-7zM16 22.127c-3.382 0-6.125-2.744-6.125-6.125s2.743-6.125 6.125-6.125c3.381 0 6.125 2.743 6.125 6.125 0 3.381-2.744 6.125-6.125 6.125zM16 12.001c-2.21 0-4 1.79-4 4s1.79 4 4 4c2.209 0 4-1.791 4-4s-1.791-4-4-4zM16 19.002c-1.656 0-3-1.344-3-3s1.344-3 3-3 3 1.344 3 3c0 1.656-1.344 3-3 3z"></path>
		</symbol>
		<symbol id="icon-tag" viewBox="0 0 32 32">
			<path d="M31.391 13.883l-5-8c-0.73-1.169-2.012-1.88-3.391-1.88h-19c-2.209 0-4 1.791-4 4v16c0 2.209 1.791 4 4 4h19c1.379 0 2.66-0.711 3.391-1.881l5-8c0.812-1.295 0.812-2.942 0-4.239zM29.695 17.062l-5 8.002c-0.367 0.588-1.002 0.939-1.695 0.939h-19c-1.103 0-2-0.898-2-2v-16c0-1.103 0.897-2 2-2h19c0.693 0 1.328 0.352 1.695 0.939l5 8c0.403 0.645 0.403 1.477 0 2.12zM23 13.003c-1.658 0-3 1.343-3 3s1.342 3 3 3c1.656 0 3-1.344 3-3 0-1.657-1.344-3-3-3zM23 18.004c-1.105 0-2-0.896-2-2s0.895-2 2-2c1.104 0 2 0.896 2 2s-0.896 2-2 2z"></path>
		</symbol>
		<symbol id="icon-pen" viewBox="0 0 32 32">
			<path d="M29.395 2.58c-1.645-1.643-3.811-2.58-5.946-2.58-1.801 0-3.459 0.668-4.67 1.877l-4.867 4.904c-0.015 0.014-0.032 0.023-0.047 0.038-0.008 0.008-0.013 0.019-0.021 0.026l0.002 0.002-10.329 10.409c-0.476 0.473-0.821 1.062-1.013 1.705l-2.349 8.508c-0.002 0.023-0.155 0.691-0.155 1.031 0 1.932 1.569 3.5 3.504 3.5 0.385 0 1.13-0.184 1.157-0.188l8.478-2.229c0.644-0.191 1.229-0.539 1.705-1.016l15.263-15.383c2.776-2.778 2.463-7.434-0.712-10.604zM16.014 23.795c-0.082-0.902-0.337-1.787-0.719-2.627l9.455-9.454c0.578 1.826 0.281 3.736-0.986 5.004-0.008 0.008-0.018 0.013-0.025 0.021l0.014 0.013-7.728 7.79c0-0.249 0.012-0.493-0.011-0.747zM14.793 20.256c-0.373-0.613-0.797-1.205-1.322-1.729-0.611-0.611-1.312-1.090-2.044-1.492l9.532-9.532c0.748 0.332 1.465 0.805 2.098 1.438 0.541 0.539 0.959 1.143 1.281 1.771l-9.545 9.544zM10.486 16.562c-0.926-0.373-1.896-0.586-2.868-0.599l7.703-7.762c1.179-1.15 2.896-1.481 4.587-1.062l-9.422 9.423zM4.167 29.873c-0.109 0.025-0.448 0.111-0.678 0.127-0.822-0.010-1.489-0.678-1.489-1.5 0.012-0.168 0.079-0.457 0.102-0.562l1.053-3.814c1.143-0.031 2.373 0.414 3.34 1.383 0.982 0.98 1.444 2.234 1.394 3.391l-3.722 0.975zM8.874 28.637c-0.024-1.342-0.57-2.738-1.672-3.838-1.042-1.043-2.406-1.645-3.766-1.699l0.996-3.607c0.072-0.24 0.215-0.477 0.391-0.684 2.006-1.436 5.091-1.012 7.234 1.133 2.267 2.266 2.617 5.586 0.871 7.568-0.116 0.061-0.233 0.119-0.359 0.156l-3.695 0.971zM28.691 11.772l-1.684 1.697c0-0.226 0.027-0.443 0.006-0.674-0.176-1.935-1.078-3.806-2.543-5.269-1.629-1.63-3.789-2.565-5.928-2.571l1.656-1.67c0.829-0.827 1.986-1.285 3.251-1.285 1.609 0 3.262 0.728 4.533 1.995 1.193 1.191 1.904 2.671 2.006 4.168 0.094 1.397-0.367 2.678-1.297 3.609z"></path>
		</symbol>
		<symbol id="icon-display" viewBox="0 0 32 32">
			<path d="M27 4.996l-22 0.004c-0.553 0-1 0.443-1 0.996v14c0 0.553 0.447 1 1 1h22c0.553 0 1-0.447 1-1v-14c0-0.553-0.447-1-1-1zM27 20h-22v-14.004h22v14.004zM29 1h-26c-1.657 0-3 1.342-3 3v20c0 1.654 1.338 2.994 2.99 2.998h10.010v1.217l-6.242 0.811c-0.446 0.111-0.758 0.511-0.758 0.97 0 0.553 0.447 1 1 1h18c0.553 0 1-0.447 1-1 0-0.459-0.312-0.859-0.758-0.971l-6.242-0.81v-1.217h10.010c1.652-0.004 2.99-1.344 2.99-2.998v-20c0-1.658-1.344-3-3-3zM30 24c0 0.551-0.449 1-1 1h-26c-0.552 0-1-0.449-1-1v-20c0-0.552 0.448-1 1-1h26c0.551 0 1 0.448 1 1v20z"></path>
		</symbol>
		<symbol id="icon-eye" viewBox="0 0 32 32">
			<path d="M31.965 15.776c-0.010-0.042-0.004-0.087-0.020-0.128-0.006-0.017-0.021-0.026-0.027-0.042-0.010-0.024-0.008-0.051-0.021-0.074-2.9-5.551-9.213-9.528-15.873-9.528-6.661 0-12.973 3.971-15.875 9.521-0.013 0.023-0.011 0.050-0.021 0.074-0.007 0.016-0.021 0.025-0.027 0.042-0.016 0.041-0.010 0.086-0.020 0.128-0.018 0.075-0.035 0.147-0.035 0.224s0.018 0.148 0.035 0.224c0.010 0.042 0.004 0.087 0.020 0.128 0.006 0.017 0.021 0.026 0.027 0.042 0.010 0.024 0.008 0.051 0.021 0.074 2.901 5.551 9.214 9.528 15.875 9.528 6.66 0 12.973-3.971 15.873-9.521 0.014-0.023 0.012-0.050 0.021-0.074 0.006-0.016 0.021-0.025 0.027-0.042 0.016-0.041 0.010-0.086 0.020-0.128 0.017-0.076 0.035-0.148 0.035-0.224s-0.018-0.149-0.035-0.224zM16.023 23.988c-5.615 0-11.112-3.191-13.851-7.995 2.754-4.81 8.243-7.99 13.851-7.99 5.613 0 11.111 3.192 13.85 7.995-2.754 4.811-8.242 7.99-13.85 7.99zM16.023 11.999c0.276 0 0.498 0.223 0.498 0.5 0 0.275-0.223 0.499-0.498 0.499v0.001c-1.654 0-2.999 1.345-2.999 2.997 0 0.276-0.224 0.5-0.499 0.5s-0.5-0.224-0.5-0.5c0-2.205 1.787-3.992 3.992-3.996 0.002 0 0.004-0.001 0.006-0.001zM16 9c-3.867 0-7 3.134-7 7s3.134 7 7 7c3.865 0 7-3.135 7-7 0-3.867-3.135-7-7-7zM16 22c-3.309 0-6-2.691-6-6s2.691-6 6-6c3.309 0 6 2.691 6 6s-2.691 6-6 6z"></path>
		</symbol>
		<symbol id="icon-bubble" viewBox="0 0 32 32">
			<path d="M16 7c0.276 0 0.5 0.224 0.5 0.5s-0.225 0.5-0.5 0.5c-5.327 0-10 2.804-10 6 0 0.276-0.224 0.5-0.5 0.5s-0.5-0.224-0.5-0.5c0-3.794 5.037-7 11-7zM16 2c-8.837 0-16 5.373-16 12 0 4.127 2.779 7.766 7.008 9.926 0 0.027-0.008 0.045-0.008 0.074 0 1.793-1.339 3.723-1.928 4.736 0.001 0 0.002 0 0.002 0-0.047 0.11-0.074 0.231-0.074 0.358 0 0.5 0.405 0.906 0.906 0.906 0.094 0 0.259-0.025 0.255-0.014 3.125-0.512 6.069-3.383 6.753-4.215 0.999 0.147 2.029 0.229 3.086 0.229 8.835 0 16-5.373 16-12s-7.164-12-16-12zM16 24c-0.917 0-1.858-0.070-2.796-0.207-0.097-0.016-0.194-0.021-0.29-0.021-0.594 0-1.163 0.264-1.546 0.73-0.428 0.521-1.646 1.684-3.085 2.539 0.39-0.895 0.695-1.898 0.716-2.932 0.006-0.064 0.009-0.129 0.009-0.184 0-0.752-0.421-1.439-1.090-1.781-3.706-1.892-5.918-4.937-5.918-8.144 0-5.514 6.28-10 14-10 7.718 0 14 4.486 14 10s-6.281 10-14 10z"></path>
		</symbol>
		<symbol id="icon-stack" viewBox="0 0 32 32">
			<path d="M31.924 18.455l-4.002-15.006c-0.242-0.853-1.033-1.449-1.922-1.449h-20c-0.89 0-1.68 0.596-1.922 1.449l-4.002 15.006c-0.051 0.182-0.076 0.365-0.076 0.545v7c0 2.209 1.791 4 4 4h24c2.209 0 4-1.791 4-4v-7c0-0.18-0.025-0.363-0.076-0.545zM30 26c0 1.102-0.898 2-2 2h-24c-1.103 0-2-0.898-2-2v-7l4.001-15.001h19.997l4.002 15.001v7zM23.742 6h-15.484c-0.453 0-0.85 0.305-0.967 0.743l-3.445 12c-0.079 0.301-0.016 0.621 0.174 0.867s0.482 0.39 0.792 0.39h4.568l1.447 2.895c0.34 0.677 1.032 1.105 1.79 1.105h6.766c0.758 0 1.449-0.428 1.789-1.105l1.447-2.895h4.568c0.311 0 0.604-0.145 0.793-0.391s0.252-0.566 0.174-0.867l-3.445-12c-0.117-0.438-0.514-0.742-0.967-0.742zM24.281 18h-1.662c-0.762 0-1.447 0.422-1.789 1.105l-1.447 2.895h-6.766l-1.447-2.895c-0.342-0.683-1.027-1.105-1.789-1.105h-4.047l2.924-11h15.484l2.924 11h-2.385z"></path>
		</symbol>
		<symbol id="icon-like" viewBox="0 0 32 32">
			<path d="M29.164 10.472c-1.25-0.328-4.189-0.324-8.488-0.438 0.203-0.938 0.25-1.784 0.25-3.286 0-3.588-2.614-6.748-4.926-6.748-1.633 0-2.979 1.335-3 2.977-0.022 2.014-0.645 5.492-4 7.256-0.246 0.13-0.95 0.477-1.053 0.522l0.053 0.045c-0.525-0.453-1.253-0.8-2-0.8h-3c-1.654 0-3 1.346-3 3v16c0 1.654 1.346 3 3 3h3c1.19 0 2.186-0.719 2.668-1.727 0.012 0.004 0.033 0.010 0.047 0.012 0.066 0.018 0.144 0.037 0.239 0.062 0.018 0.005 0.027 0.007 0.046 0.012 0.576 0.143 1.685 0.408 4.055 0.953 0.508 0.116 3.192 0.688 5.972 0.688h5.467c1.666 0 2.867-0.641 3.582-1.928 0.010-0.020 0.24-0.469 0.428-1.076 0.141-0.457 0.193-1.104 0.023-1.76 1.074-0.738 1.42-1.854 1.645-2.58 0.377-1.191 0.264-2.086 0.002-2.727 0.604-0.57 1.119-1.439 1.336-2.766 0.135-0.822-0.010-1.668-0.389-2.372 0.566-0.636 0.824-1.436 0.854-2.176l0.012-0.209c0.007-0.131 0.013-0.212 0.013-0.5 0-1.263-0.875-2.874-2.836-3.434zM7 29c0 0.553-0.447 1-1 1h-3c-0.553 0-1-0.447-1-1v-16c0-0.553 0.447-1 1-1h3c0.553 0 1 0.447 1 1v16zM29.977 14.535c-0.020 0.494-0.227 1.465-1.977 1.465-1.5 0-2 0-2 0-0.277 0-0.5 0.224-0.5 0.5s0.223 0.5 0.5 0.5c0 0 0.438 0 1.938 0s1.697 1.244 1.6 1.844c-0.124 0.746-0.474 2.156-2.163 2.156-1.687 0-2.375 0-2.375 0-0.277 0-0.5 0.223-0.5 0.5 0 0.275 0.223 0.5 0.5 0.5 0 0 1.188 0 1.969 0 1.688 0 1.539 1.287 1.297 2.055-0.319 1.009-0.514 1.945-2.641 1.945-0.719 0-1.631 0-1.631 0-0.277 0-0.5 0.223-0.5 0.5 0 0.275 0.223 0.5 0.5 0.5 0 0 0.693 0 1.568 0 1.094 0 1.145 1.035 1.031 1.406-0.125 0.406-0.273 0.707-0.279 0.721-0.302 0.545-0.789 0.873-1.82 0.873h-5.467c-2.746 0-5.47-0.623-5.54-0.639-4.154-0.957-4.373-1.031-4.634-1.105 0 0-0.846-0.143-0.846-0.881l-0.007-13.812c0-0.469 0.299-0.893 0.794-1.042 0.062-0.024 0.146-0.050 0.206-0.075 4.568-1.892 5.959-6.040 6-9.446 0.006-0.479 0.375-1 1-1 1.057 0 2.926 2.122 2.926 4.748 0 2.371-0.096 2.781-0.926 5.252 10 0 9.93 0.144 10.812 0.375 1.094 0.313 1.188 1.219 1.188 1.531 0 0.343-0.010 0.293-0.023 0.629zM4.5 26c-0.828 0-1.5 0.672-1.5 1.5s0.672 1.5 1.5 1.5 1.5-0.672 1.5-1.5-0.672-1.5-1.5-1.5zM4.5 28c-0.275 0-0.5-0.225-0.5-0.5s0.225-0.5 0.5-0.5 0.5 0.225 0.5 0.5-0.225 0.5-0.5 0.5z"></path>
		</symbol>
		<symbol id="icon-note" viewBox="0 0 32 32">
			<path d="M31.414 7.585l-6-6c-0.375-0.375-0.885-0.585-1.414-0.585h-21c-1.654 0-3 1.345-3 3v24c0 1.654 1.346 3 3 3h26c1.654 0 3-1.346 3-3v-19c0-0.531-0.211-1.040-0.586-1.415zM30 28c0 0.553-0.447 1-1 1h-26c-0.553 0-1-0.447-1-1v-24c0-0.553 0.447-1 1-1h20v4h-0.002c0 1.657 1.344 3 3 3h4.002v18zM26.998 9h-1c-1.102 0-2-0.897-2-2h0.002v-4l6 6h-3.002zM15.5 8c-0.276 0-0.5-0.223-0.5-0.5s0.224-0.5 0.5-0.5h5c0.275 0 0.5 0.224 0.5 0.5s-0.225 0.5-0.5 0.5h-5zM15.5 11c-0.276 0-0.5-0.224-0.5-0.5s0.224-0.5 0.5-0.5h5c0.275 0 0.5 0.224 0.5 0.5s-0.225 0.5-0.5 0.5h-5zM15 13.5c0-0.277 0.224-0.5 0.5-0.5h12c0.275 0 0.5 0.224 0.5 0.5s-0.225 0.5-0.5 0.5h-12c-0.276 0-0.5-0.224-0.5-0.5zM27.5 19c0.275 0 0.5 0.223 0.5 0.5 0 0.275-0.225 0.5-0.5 0.5h-23c-0.276 0-0.5-0.225-0.5-0.5 0-0.277 0.224-0.5 0.5-0.5h23zM27.5 22c0.275 0 0.5 0.223 0.5 0.5 0 0.275-0.225 0.5-0.5 0.5h-23c-0.276 0-0.5-0.225-0.5-0.5 0-0.277 0.224-0.5 0.5-0.5h23zM27.5 25c0.275 0 0.5 0.223 0.5 0.5 0 0.275-0.225 0.5-0.5 0.5h-23c-0.276 0-0.5-0.225-0.5-0.5 0-0.277 0.224-0.5 0.5-0.5h23zM27.5 16c0.275 0 0.5 0.224 0.5 0.5s-0.225 0.5-0.5 0.5h-23c-0.276 0-0.5-0.223-0.5-0.5s0.224-0.5 0.5-0.5h23zM5 14h7c0.553 0 1-0.447 1-1v-6c0-0.553-0.447-1-1-1h-7c-0.553 0-1 0.447-1 1v6c0 0.552 0.447 1 1 1zM6 8h5v4h-5v-4z"></path>
		</symbol>
		<symbol id="icon-paperplane" viewBox="0 0 32 32">
			<path d="M31.543 0.16c-0.166-0.107-0.355-0.16-0.543-0.16-0.193 0-0.387 0.055-0.555 0.168l-30 20c-0.309 0.205-0.479 0.566-0.439 0.936 0.038 0.369 0.278 0.688 0.623 0.824l7.824 3.131 3.679 6.438c0.176 0.309 0.503 0.5 0.857 0.504 0.004 0 0.007 0 0.011 0 0.351 0 0.677-0.186 0.857-0.486l2.077-3.463 9.695 3.877c0.119 0.048 0.244 0.071 0.371 0.071 0.17 0 0.338-0.043 0.49-0.129 0.264-0.148 0.445-0.408 0.496-0.707l5-30c0.065-0.393-0.109-0.787-0.443-1.004zM3.136 20.777l23.175-15.451-16.85 18.037c-0.089-0.053-0.168-0.123-0.266-0.162l-6.059-2.424zM10.189 24.066c-0.002-0.004-0.005-0.006-0.007-0.010l18.943-20.275-16.149 25.162-2.787-4.877zM25.217 29.609l-8.541-3.416c-0.203-0.080-0.414-0.107-0.623-0.119l13.152-20.388-3.988 23.923z"></path>
		</symbol>
		<symbol id="icon-params" viewBox="0 0 32 32">
			<path d="M8 6.021v-3.021c0-1.654-1.346-3-3-3s-3 1.346-3 3v3.021c-1.208 0.915-2 2.348-2 3.979s0.792 3.064 2 3.977v15.023c0 1.654 1.346 3 3 3s3-1.346 3-3v-15.023c1.208-0.912 2-2.346 2-3.977s-0.792-3.064-2-3.979zM4 3c0-0.553 0.447-1 1-1s1 0.447 1 1v2.1c-0.323-0.065-0.657-0.1-1-0.1s-0.677 0.035-1 0.1v-2.1zM6 29c0 0.553-0.447 1-1 1s-1-0.447-1-1v-14.102c0.323 0.067 0.657 0.102 1 0.102s0.677-0.035 1-0.102v14.102zM7.865 10.84c-0.016 0.053-0.030 0.105-0.049 0.158-0.095 0.264-0.217 0.514-0.378 0.736-0.004 0.006-0.010 0.010-0.014 0.016-0.174 0.238-0.381 0.449-0.616 0.627-0.004 0.004-0.007 0.006-0.010 0.008-0.241 0.182-0.51 0.328-0.799 0.43-0.313 0.113-0.646 0.185-0.999 0.185s-0.686-0.072-1-0.186c-0.289-0.102-0.558-0.248-0.799-0.43-0.003-0.002-0.006-0.004-0.010-0.008-0.235-0.178-0.442-0.389-0.616-0.627-0.004-0.006-0.010-0.010-0.014-0.016-0.161-0.223-0.283-0.473-0.378-0.736-0.019-0.053-0.033-0.105-0.049-0.158-0.079-0.267-0.134-0.546-0.134-0.839 0-0.295 0.055-0.574 0.135-0.842 0.016-0.053 0.030-0.105 0.049-0.156 0.094-0.264 0.216-0.514 0.378-0.738 0.004-0.006 0.010-0.010 0.014-0.016 0.174-0.236 0.381-0.449 0.616-0.627 0.004-0.002 0.007-0.006 0.010-0.008 0.24-0.179 0.509-0.326 0.798-0.429 0.314-0.112 0.647-0.184 1-0.184s0.686 0.072 1 0.184c0.289 0.104 0.558 0.25 0.799 0.43 0.003 0.002 0.006 0.006 0.010 0.008 0.235 0.178 0.442 0.391 0.616 0.627 0.004 0.006 0.010 0.010 0.014 0.016 0.161 0.223 0.283 0.473 0.377 0.737 0.019 0.051 0.034 0.103 0.049 0.156 0.080 0.268 0.135 0.547 0.135 0.842 0 0.293-0.055 0.572-0.135 0.84zM30 6.021v-3.021c0-1.654-1.346-3-3-3s-3 1.346-3 3v3.021c-1.209 0.915-2 2.348-2 3.979s0.791 3.064 2 3.977v15.023c0 1.654 1.346 3 3 3s3-1.346 3-3v-15.023c1.207-0.912 2-2.346 2-3.977s-0.793-3.064-2-3.979zM26 3c0-0.553 0.447-1 1-1s1 0.447 1 1v2.1c-0.324-0.065-0.658-0.1-1-0.1-0.344 0-0.678 0.035-1 0.1v-2.1zM28 29c0 0.553-0.447 1-1 1s-1-0.447-1-1v-14.102c0.322 0.067 0.656 0.102 1 0.102 0.342 0 0.676-0.035 1-0.102v14.102zM29.865 10.84c-0.016 0.053-0.031 0.105-0.049 0.158-0.096 0.264-0.217 0.514-0.379 0.736-0.004 0.006-0.010 0.010-0.014 0.016-0.174 0.238-0.381 0.449-0.615 0.627-0.004 0.004-0.008 0.006-0.010 0.008-0.242 0.182-0.51 0.328-0.799 0.43-0.313 0.113-0.647 0.185-0.999 0.185-0.354 0-0.686-0.072-1-0.186-0.289-0.102-0.559-0.248-0.799-0.43-0.004-0.002-0.006-0.004-0.010-0.008-0.236-0.178-0.443-0.389-0.617-0.627-0.004-0.006-0.010-0.010-0.014-0.016-0.16-0.223-0.283-0.473-0.377-0.736-0.020-0.053-0.033-0.105-0.049-0.158-0.079-0.267-0.134-0.546-0.134-0.839 0-0.295 0.055-0.574 0.135-0.842 0.016-0.053 0.029-0.105 0.049-0.156 0.094-0.264 0.217-0.514 0.377-0.738 0.004-0.006 0.010-0.010 0.014-0.016 0.174-0.236 0.381-0.449 0.617-0.627 0.004-0.002 0.006-0.006 0.010-0.008 0.24-0.18 0.51-0.326 0.799-0.43 0.313-0.111 0.645-0.183 0.999-0.183 0.352 0 0.686 0.072 1 0.184 0.289 0.104 0.557 0.25 0.799 0.43 0.002 0.002 0.006 0.006 0.010 0.008 0.234 0.178 0.441 0.391 0.615 0.627 0.004 0.006 0.010 0.010 0.014 0.016 0.162 0.225 0.283 0.475 0.379 0.738 0.018 0.051 0.033 0.104 0.049 0.156 0.079 0.267 0.134 0.546 0.134 0.841 0 0.293-0.055 0.572-0.135 0.84zM19 18.021v-15.021c0-1.654-1.346-3-3-3s-3 1.346-3 3v15.021c-1.208 0.914-2 2.348-2 3.979s0.792 3.064 2 3.977v3.023c0 1.654 1.346 3 3 3s3-1.346 3-3v-3.023c1.207-0.912 2-2.346 2-3.977s-0.793-3.064-2-3.979zM15 3c0-0.553 0.447-1 1-1s1 0.447 1 1v14.1c-0.324-0.064-0.658-0.1-1-0.1-0.343 0-0.677 0.035-1 0.1v-14.1zM17 29c0 0.553-0.447 1-1 1s-1-0.447-1-1v-2.102c0.323 0.067 0.657 0.102 1 0.102 0.342 0 0.676-0.035 1-0.102v2.102zM18.865 22.84c-0.016 0.053-0.031 0.105-0.049 0.158-0.096 0.264-0.217 0.514-0.379 0.736-0.004 0.006-0.010 0.010-0.014 0.016-0.174 0.238-0.381 0.449-0.615 0.627-0.004 0.004-0.008 0.006-0.010 0.008-0.242 0.182-0.51 0.328-0.799 0.43-0.313 0.113-0.647 0.185-0.999 0.185s-0.686-0.072-1-0.186c-0.289-0.102-0.558-0.248-0.799-0.43-0.003-0.002-0.006-0.004-0.010-0.008-0.235-0.178-0.442-0.389-0.616-0.627-0.004-0.006-0.010-0.010-0.014-0.016-0.161-0.223-0.283-0.473-0.378-0.736-0.019-0.053-0.033-0.105-0.049-0.158-0.079-0.267-0.134-0.546-0.134-0.839 0-0.295 0.055-0.574 0.135-0.842 0.016-0.053 0.030-0.105 0.049-0.156 0.095-0.264 0.217-0.514 0.378-0.738 0.004-0.006 0.010-0.010 0.014-0.016 0.174-0.236 0.381-0.449 0.616-0.627 0.004-0.002 0.007-0.006 0.010-0.008 0.241-0.18 0.51-0.326 0.799-0.43 0.313-0.111 0.646-0.183 0.999-0.183 0.352 0 0.686 0.072 1 0.184 0.289 0.104 0.557 0.25 0.799 0.43 0.002 0.002 0.006 0.006 0.010 0.008 0.234 0.178 0.441 0.391 0.615 0.627 0.004 0.006 0.010 0.010 0.014 0.016 0.162 0.225 0.283 0.475 0.379 0.738 0.018 0.051 0.033 0.104 0.049 0.156 0.079 0.267 0.134 0.546 0.134 0.841 0 0.293-0.055 0.572-0.135 0.84z"></path>
		</symbol>
		<symbol id="icon-data" viewBox="0 0 32 32">
			<path d="M16 0c-6.744 0-14 2.033-14 6.5v19c0 4.465 7.256 6.5 14 6.5s14-2.035 14-6.5v-19c0-4.467-7.258-6.5-14-6.5zM28 25.5c0 2.484-5.373 4.5-12 4.5-6.628 0-12-2.016-12-4.5v-3.736c2.066 2.129 7.050 3.236 12 3.236s9.934-1.107 12-3.236v3.736zM28 19.5h-0.004c0 0.010 0.004 0.021 0.004 0.031 0 2.469-5.373 4.469-12 4.469s-12-2-12-4.469c0-0.010 0.004-0.021 0.004-0.031h-0.004v-3.736c2.066 2.129 7.050 3.236 12 3.236s9.934-1.107 12-3.236v3.736zM28 13.5h-0.004c0 0.010 0.004 0.021 0.004 0.031 0 2.469-5.373 4.469-12 4.469s-12-2-12-4.469c0-0.010 0.004-0.021 0.004-0.031h-0.004v-3.436c2.621 1.997 7.425 2.936 12 2.936s9.379-0.939 12-2.936v3.436zM16 11c-6.628 0-12-2.016-12-4.5 0-2.486 5.372-4.5 12-4.5 6.627 0 12 2.014 12 4.5 0 2.484-5.373 4.5-12 4.5zM24 26c0-0.552 0.448-1 1-1s1 0.448 1 1c0 0.552-0.448 1-1 1s-1-0.448-1-1zM24 20c0-0.552 0.448-1 1-1s1 0.448 1 1c0 0.552-0.448 1-1 1s-1-0.448-1-1zM24 14c0-0.552 0.448-1 1-1s1 0.448 1 1c0 0.552-0.448 1-1 1s-1-0.448-1-1z"></path>
		</symbol>
		<symbol id="icon-lab" viewBox="0 0 32 32">
			<path d="M20.682 3.732c-0.473-0.472-1.1-0.732-1.768-0.732s-1.295 0.26-1.77 0.733l-1.41 1.412c-0.473 0.472-0.734 1.1-0.734 1.769 0 0.471 0.129 0.922 0.371 1.313l-13.577 5.439c-0.908 0.399-1.559 1.218-1.742 2.189-0.185 0.977 0.125 1.979 0.834 2.687l12.72 12.58c0.548 0.548 1.276 0.859 2.045 0.877 0.018 0.001 0.060 0.001 0.078 0.001 0.202 0 0.407-0.021 0.61-0.062 0.994-0.206 1.808-0.893 2.177-1.828l5.342-13.376c0.402 0.265 0.875 0.407 1.367 0.407 0.67 0 1.297-0.261 1.768-0.733l1.407-1.408c0.477-0.474 0.738-1.103 0.738-1.773s-0.262-1.3-0.732-1.768l-7.724-7.727zM16.659 29.367c-0.124 0.313-0.397 0.544-0.727 0.612-0.076 0.016-0.153 0.022-0.229 0.021-0.254-0.006-0.499-0.108-0.682-0.292l-12.728-12.588c-0.234-0.233-0.337-0.567-0.275-0.893 0.061-0.324 0.279-0.598 0.582-0.73l6.217-2.49c4.189 1.393 8.379 0.051 12.57 4.522l-4.728 11.838zM26.992 13.58l-1.414 1.413c-0.195 0.196-0.512 0.196-0.707 0l-1.768-1.767-1.432 3.589 0.119-0.303c-3.010-3.005-6.069-3.384-8.829-3.723-0.887-0.109-1.747-0.223-2.592-0.405l8.491-3.401-1.715-1.715c-0.195-0.195-0.195-0.512 0-0.707l1.414-1.415c0.195-0.195 0.512-0.195 0.707 0l7.725 7.727c0.198 0.195 0.198 0.512 0.001 0.707zM16.5 21c1.378 0 2.5-1.121 2.5-2.5s-1.121-2.5-2.5-2.5c-1.379 0-2.5 1.121-2.5 2.5s1.122 2.5 2.5 2.5zM16.5 17c0.828 0 1.5 0.672 1.5 1.5s-0.672 1.5-1.5 1.5c-0.829 0-1.5-0.672-1.5-1.5s0.671-1.5 1.5-1.5zM29.5 0c-1.379 0-2.5 1.121-2.5 2.5s1.121 2.5 2.5 2.5 2.5-1.121 2.5-2.5-1.121-2.5-2.5-2.5zM29.5 4c-0.828 0-1.5-0.672-1.5-1.5s0.672-1.5 1.5-1.5 1.5 0.672 1.5 1.5-0.672 1.5-1.5 1.5zM8 17c0 1.103 0.897 2 2 2s2-0.897 2-2-0.897-2-2-2-2 0.897-2 2zM10 16c0.552 0 1 0.447 1 1s-0.448 1-1 1-1-0.447-1-1 0.448-1 1-1zM12 23c0-0.552 0.448-1 1-1s1 0.448 1 1c0 0.552-0.448 1-1 1s-1-0.448-1-1zM28 8c0-0.552 0.448-1 1-1s1 0.448 1 1c0 0.552-0.448 1-1 1s-1-0.448-1-1z"></path>
		</symbol>
		<symbol id="icon-calendar" viewBox="0 0 32 32">
			<path d="M29.334 3h-4.334v-2c0-0.553-0.447-1-1-1s-1 0.447-1 1v2h-6v-2c0-0.553-0.448-1-1-1s-1 0.447-1 1v2h-6v-2c0-0.553-0.448-1-1-1s-1 0.447-1 1v2h-4.333c-1.473 0-2.667 1.193-2.667 2.666v23.667c0 1.473 1.194 2.667 2.667 2.667h26.667c1.473 0 2.666-1.194 2.666-2.667v-23.667c0-1.473-1.193-2.666-2.666-2.666zM30 29.333c0 0.368-0.299 0.667-0.666 0.667h-26.667c-0.368 0-0.667-0.299-0.667-0.667v-23.667c0-0.367 0.299-0.666 0.667-0.666h4.333v2c0 0.553 0.448 1 1 1s1-0.447 1-1v-2h6v2c0 0.553 0.448 1 1 1s1-0.447 1-1v-2h6v2c0 0.553 0.447 1 1 1s1-0.447 1-1v-2h4.334c0.367 0 0.666 0.299 0.666 0.666v23.667zM7 12h4v3h-4zM7 17h4v3h-4zM7 22h4v3h-4zM14 22h4v3h-4zM14 17h4v3h-4zM14 12h4v3h-4zM21 22h4v3h-4zM21 17h4v3h-4zM21 12h4v3h-4z"></path>
		</symbol>
		<symbol id="icon-copy" viewBox="0 0 32 32">
			<path d="M20 8v-8h-14l-6 6v18h12v8h20v-24h-12zM6 2.828v3.172h-3.172l3.172-3.172zM2 22v-14h6v-6h10v6l-6 6v8h-10zM18 10.828v3.172h-3.172l3.172-3.172zM30 30h-16v-14h6v-6h10v20z"></path>
		</symbol>
		<symbol id="icon-stack2" viewBox="0 0 32 32">
			<path d="M32 10l-16-8-16 8 16 8 16-8zM16 4.655l10.689 5.345-10.689 5.345-10.689-5.345 10.689-5.345zM28.795 14.398l3.205 1.602-16 8-16-8 3.205-1.602 12.795 6.398zM28.795 20.398l3.205 1.602-16 8-16-8 3.205-1.602 12.795 6.398z"></path>
		</symbol>
		<symbol id="icon-loop2" viewBox="0 0 32 32">
			<path d="M27.802 5.197c-2.925-3.194-7.13-5.197-11.803-5.197-8.837 0-16 7.163-16 16h3c0-7.18 5.82-13 13-13 3.844 0 7.298 1.669 9.678 4.322l-4.678 4.678h11v-11l-4.198 4.197z"></path>
			<path d="M29 16c0 7.18-5.82 13-13 13-3.844 0-7.298-1.669-9.678-4.322l4.678-4.678h-11v11l4.197-4.197c2.925 3.194 7.13 5.197 11.803 5.197 8.837 0 16-7.163 16-16h-3z"></path>
		</symbol>
		<symbol id="icon-insert-template" viewBox="0 0 32 32">
			<path d="M12 6h4v2h-4zM18 6h4v2h-4zM28 6v8h-6v-2h4v-4h-2v-2zM10 12h4v2h-4zM16 12h4v2h-4zM6 8v4h2v2h-4v-8h6v2zM12 18h4v2h-4zM18 18h4v2h-4zM28 18v8h-6v-2h4v-4h-2v-2zM10 24h4v2h-4zM16 24h4v2h-4zM6 20v4h2v2h-4v-8h6v2zM30 2h-28v28h28v-28zM32 0v0 32h-32v-32h32z"></path>
		</symbol>
		<symbol id="icon-folder-o" viewBox="0 0 26 28">
			<path d="M24 20.5v-11c0-0.828-0.672-1.5-1.5-1.5h-11c-0.828 0-1.5-0.672-1.5-1.5v-1c0-0.828-0.672-1.5-1.5-1.5h-5c-0.828 0-1.5 0.672-1.5 1.5v15c0 0.828 0.672 1.5 1.5 1.5h19c0.828 0 1.5-0.672 1.5-1.5zM26 9.5v11c0 1.922-1.578 3.5-3.5 3.5h-19c-1.922 0-3.5-1.578-3.5-3.5v-15c0-1.922 1.578-3.5 3.5-3.5h5c1.922 0 3.5 1.578 3.5 3.5v0.5h10.5c1.922 0 3.5 1.578 3.5 3.5z"></path>
		</symbol>
		<symbol id="icon-code" viewBox="0 0 30 28">
			<path d="M9.641 21.859l-0.781 0.781c-0.203 0.203-0.516 0.203-0.719 0l-7.281-7.281c-0.203-0.203-0.203-0.516 0-0.719l7.281-7.281c0.203-0.203 0.516-0.203 0.719 0l0.781 0.781c0.203 0.203 0.203 0.516 0 0.719l-6.141 6.141 6.141 6.141c0.203 0.203 0.203 0.516 0 0.719zM18.875 5.187l-5.828 20.172c-0.078 0.266-0.359 0.422-0.609 0.344l-0.969-0.266c-0.266-0.078-0.422-0.359-0.344-0.625l5.828-20.172c0.078-0.266 0.359-0.422 0.609-0.344l0.969 0.266c0.266 0.078 0.422 0.359 0.344 0.625zM29.141 15.359l-7.281 7.281c-0.203 0.203-0.516 0.203-0.719 0l-0.781-0.781c-0.203-0.203-0.203-0.516 0-0.719l6.141-6.141-6.141-6.141c-0.203-0.203-0.203-0.516 0-0.719l0.781-0.781c0.203-0.203 0.516-0.203 0.719 0l7.281 7.281c0.203 0.203 0.203 0.516 0 0.719z"></path>
		</symbol>
		<symbol id="checkmark-outline" viewBox="0 0 20 20">
			<path d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM6.7 9.29L9 11.6l4.3-4.3 1.4 1.42L9 14.4l-3.7-3.7 1.4-1.42z"/>
		</symbol>
	</svg>

</main><!-- #main -->

<script>
( function( document ) {
	function Countdown( el ) {
		this.el = el;

		this.days = this.el.querySelector( '.countdown-days' );
		this.hours = this.el.querySelector( '.countdown-hours' );
		this.minutes = this.el.querySelector( '.countdown-minutes' );
		this.seconds = this.el.querySelector( '.countdown-seconds' );

		this.countdown = function() {
			var time = new Date( this.el.dataset.end ).getTime();
			var now = new Date().getTime();
			var distance = ( time - now ) / 1000;

			if ( distance < 0 ) {
				this.days.innerHTML = '0';
				this.hours.innerHTML = '0';
				this.minutes.innerHTML = '0';
				this.seconds.innerHTML = '0';
				clearInterval( this.timer );
				return;
			}

			this.days.innerHTML = Math.floor(distance / 86400);
			this.hours.innerHTML = Math.floor((distance % 86400) / 3600);
			this.minutes.innerHTML = Math.floor((distance % 3600) / 60);
			this.seconds.innerHTML = Math.floor(distance % 60);
		};

		this.timer = setInterval( this.countdown.bind( this ), 1000 );
	}

	document.querySelectorAll( '.countdown' ).forEach( function( el ) {
		new Countdown( el );
	} );
} )( document );
</script>
<?php
get_footer();
