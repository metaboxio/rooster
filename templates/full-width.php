<?php
/**
 * Template Name: Full Width
 */

get_header();
the_post();
?>

<div class="page-header">
	<div class="container">
		<?php the_title( '<h1 class="page-title">', '</h1>' ); ?>
		<?php
		if ( has_excerpt() ) {
			the_excerpt();
		}
		?>
	</div>
</div>

<main class="container">
	<?php the_content(); ?>
</main>

<?php
get_footer();

