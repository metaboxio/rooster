<?php
/**
 * Template Name: Landing (No Container)
 */

get_header();

the_post();
the_content();

get_footer();
